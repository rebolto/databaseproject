SET SERVEROUTPUT ON;
DROP TABLE AuditLog CASCADE CONSTRAINTS;
DROP TABLE Province CASCADE CONSTRAINTS;
DROP TABLE City CASCADE CONSTRAINTS;
DROP TABLE Address CASCADE CONSTRAINTS;
DROP TABLE Warehouse CASCADE CONSTRAINTS;
DROP TABLE Customer CASCADE CONSTRAINTS;
DROP TABLE Product CASCADE CONSTRAINTS;
DROP TABLE Stores CASCADE CONSTRAINTS;
DROP TABLE Stores_Product CASCADE CONSTRAINTS;
DROP TABLE Availability_Warehouse_Product CASCADE CONSTRAINTS;
DROP TABLE Review CASCADE CONSTRAINTS;
DROP TABLE Orders CASCADE CONSTRAINTS;
DROP TABLE Orders_Product CASCADE CONSTRAINTS;

CREATE TABLE AuditLog(
LogId Number(4) GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
action VARCHAR2(50),
action_time VARCHAR2(50),
table_modified VARCHAR2(50),
id NUMBER(4)
);

CREATE TABLE Province(
    ProvinceId NUMBER(4)    GENERATED ALWAYS AS IDENTITY PRIMARY KEY ,
    Province   VARCHAR2(50) NOT NULL
);
INSERT INTO Province ( Province ) VALUES('Ontario');
INSERT INTO Province ( Province ) VALUES('Quebec');
INSERT INTO Province ( Province ) VALUES('Alberta');



CREATE TABLE City(
    CityId     NUMBER(4) GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    City       VARCHAR2(50) NOT NULL,
    ProvinceId NUMBER(4) REFERENCES Province(ProvinceId) 
);

INSERT INTO City ( City, ProvinceId ) VALUES('Toronto', (SELECT ProvinceId FROM province where(province='Ontario')));
INSERT INTO City ( City, ProvinceId ) VALUES('Montreal', (SELECT ProvinceId FROM province where(province='Quebec')));
INSERT INTO City ( City, ProvinceId ) VALUES('Calgary', (SELECT ProvinceId FROM province where(province='Alberta')));
INSERT INTO City ( City, ProvinceId ) VALUES('Brossard', (SELECT ProvinceId FROM province where(province='Quebec')));
INSERT INTO City ( City, ProvinceId ) VALUES('Laval', (SELECT ProvinceId FROM province where(province='Quebec')));
INSERT INTO City ( City, ProvinceId ) VALUES('Saint Laurent', (SELECT ProvinceId FROM province where(province='Quebec')));
INSERT INTO City ( City, ProvinceId ) VALUES('Quebec City', (SELECT ProvinceId FROM province where(province='Quebec')));
INSERT INTO City ( City, ProvinceId ) VALUES('Ottawa', (SELECT ProvinceId FROM province where(province='Ontario')));

CREATE TABLE Address (
    AddressId NUMBER(4)     GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    Street    VARCHAR2(50),
    CityId    NUMBER(4)     REFERENCES City(CityId),
    Country   VARCHAR2(50)  NOT NULL
);

CREATE OR REPLACE TYPE ADDRESS_TYP AS OBJECT (
    Street    VARCHAR2(50),
    City      VARCHAR2(50),
    Province  VARCHAR2(50),
    Country   VARCHAR2(50)
);
/

CREATE OR REPLACE PROCEDURE add_complete_address(
    p_street IN VARCHAR2,
    p_city IN VARCHAR2,
    p_province IN VARCHAR2,
    p_country IN VARCHAR2,
    p_address_id OUT NUMBER
) IS
    v_city_id City.CityId%TYPE;
    v_province_id Province.ProvinceId%TYPE;
BEGIN
    BEGIN
        SELECT ProvinceId INTO v_province_id FROM Province WHERE Province = p_province;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            INSERT INTO Province (Province) VALUES (p_province) RETURNING ProvinceId INTO v_province_id;
    END;
    
    BEGIN
        SELECT CityId INTO v_city_id FROM City WHERE City = p_city AND ProvinceId = v_province_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            INSERT INTO City (City, ProvinceId) VALUES (p_city, v_province_id) RETURNING CityId INTO v_city_id;
    END;
    
    INSERT INTO Address (Street, CityId, Country) VALUES (p_street, v_city_id, p_country) RETURNING AddressId INTO p_address_id;
END add_complete_address;
/

INSERT INTO Address ( Street, CityId, Country ) VALUES('100 Atwater Street', (SELECT CityId FROM city where(city='Toronto')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES('100 Young Street', (SELECT CityId FROM city where(city='Toronto')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES('100 Saint Laurent', (SELECT CityId FROM city where(city='Montreal')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES(NULL, (SELECT CityId FROM city where(city='Calgary')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES(NULL, (SELECT CityId FROM city where(city='Brossard')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES('104 Gill Street', (SELECT CityId FROM city where(city='Toronto')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES('105 Young Street', (SELECT CityId FROM city where(city='Toronto')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES('90 boul Saint Laurent', (SELECT CityId FROM city where(city='Montreal')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES('87 boul Saint Laurent', (SELECT CityId FROM city where(city='Montreal')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES('76 boul Decalthon', (SELECT CityId FROM city where(city='Laval')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES('22222 Happy Street', (SELECT CityId FROM city where(city='Laval')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES('Dawson College', (SELECT CityId FROM city where(city='Montreal')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES('100 rue William', (SELECT CityId FROM city where(city='Saint Laurent')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES('304 Rue Franois-Perrault', (SELECT CityId FROM city where(city='Montreal')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES('86700 Weston Rd', (SELECT CityId FROM city where(city='Toronto')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES('170  Sideroad', (SELECT CityId FROM city where(city='Quebec City')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES('1231 Trudea road', (SELECT CityId FROM city where(city='Ottawa')), 'Canada');
INSERT INTO Address ( Street, CityId, Country ) VALUES('16  Whitlock Rd', (SELECT CityId FROM city where(city='Calgary')), 'Canada');

CREATE TABLE Warehouse (
    WarehouseId   NUMBER(4)    GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    WarehouseName VARCHAR2(50) NOT NULL,
    AddressId     NUMBER(4)   REFERENCES Address(AddressId) 
);
CREATE OR REPLACE TRIGGER track_changes_on_warehouse_table
    AFTER INSERT OR DELETE OR UPDATE ON WAREHOUSE
        FOR EACH ROW
        DECLARE
            actions Varchar2(20);
        BEGIN
            IF INSERTING THEN
                actions := 'Insert';
                INSERT INTO AuditLog(action,action_time,table_modified,id) VALUES (actions,SYSDATE,'WAREHOUSE',:NEW.WAREHOUSEID);
            ELSIF UPDATING THEN
                actions := 'Update';
                INSERT INTO AuditLog(action,action_time,table_modified,id) VALUES (actions,SYSDATE,'WAREHOUSE',:NEW.WAREHOUSEID);
            ELSIF DELETING THEN
                actions := 'Delete';
                INSERT INTO AuditLog(action,action_time,table_modified,id) VALUES (actions,SYSDATE,'WAREHOUSE',:OLD.WAREHOUSEID);
            END IF;
        END;
/

INSERT INTO Warehouse ( WarehouseName,AddressId ) VALUES ('Warehouse A',(Select addressId FROM ADDRESS WHERE (street='100 rue William')));
INSERT INTO Warehouse ( WarehouseName,AddressId ) VALUES ('Warehouse B',(Select addressId FROM ADDRESS WHERE (street='304 Rue Fran�ois-Perrault')));
INSERT INTO Warehouse ( WarehouseName,AddressId ) VALUES ('Warehouse C',(Select addressId FROM ADDRESS WHERE (street='86700 Weston Rd')));
INSERT INTO Warehouse ( WarehouseName,AddressId ) VALUES ('Warehouse D', (Select addressId FROM ADDRESS WHERE (street='170  Sideroad')));
INSERT INTO Warehouse ( WarehouseName,AddressId ) VALUES ('Warehouse E', (Select addressId FROM ADDRESS WHERE (street='1231 Trudea road')));
INSERT INTO Warehouse ( WarehouseName,AddressId ) VALUES ('Warehouse F', (Select addressId FROM ADDRESS WHERE (street='16  Whitlock Rd')));
INSERT INTO Warehouse ( WarehouseName,AddressId ) VALUES ('Warehouse G', (Select addressId FROM ADDRESS WHERE (street='16  Whitlock Rd')));

CREATE TABLE Product(
    ProductId       NUMBER(4)     GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    ProductName     VARCHAR2(30)  NOT NULL,
    ProductCategory VARCHAR2(200) NOT NULL
);
CREATE OR REPLACE TYPE PRODUCT_TYP AS OBJECT(
   ProductId       NUMBER(4),
    ProductName     VARCHAR2(30),
    ProductCategory VARCHAR2(200)
);
/

 CREATE OR REPLACE PROCEDURE add_product (newProduct IN product_typ)
    IS
        BEGIN
        INSERT INTO PRODUCT ( productName, productCategory ) VALUES (
        newProduct.productName,
        newProduct.productCategory);
END add_product ;  
/
CREATE OR REPLACE TRIGGER track_changes_on_products_table
    AFTER INSERT OR DELETE OR UPDATE ON PRODUCT
        FOR EACH ROW
        DECLARE
            actions Varchar2(20);
        BEGIN
            IF INSERTING THEN
                actions := 'Insert';
                INSERT INTO AuditLog(action,action_time,table_modified,id) VALUES (actions,SYSDATE,'PRODUCT',:NEW.PRODUCTID);
            ELSIF UPDATING THEN
                actions := 'Update';
                INSERT INTO AuditLog(action,action_time,table_modified,id) VALUES (actions,SYSDATE,'PRODUCT',:NEW.PRODUCTID);
            ELSIF DELETING THEN
                actions := 'Delete';
                INSERT INTO AuditLog(action,action_time,table_modified,id) VALUES (actions,SYSDATE,'PRODUCT',:OLD.PRODUCTID);
            END IF;
        END;
/

INSERT INTO product ( productName,ProductCategory ) VALUES ('Laptop ASUS 104S','Electronics'); 
INSERT INTO product ( productName,ProductCategory ) VALUES ('Apple','Grocery');
INSERT INTO product ( productName, ProductCategory ) VALUES ('Sims Cd','Video Games'); 
INSERT INTO product ( productName, ProductCategory ) VALUES ('Orange','Grocery');  
INSERT INTO product ( productName, ProductCategory ) VALUES ('Barbie Movie','DVD'); 
INSERT INTO product ( productName, ProductCategory ) VALUES ('L''Oreal Normal Hair','Health');
INSERT INTO product ( productName, ProductCategory ) VALUES ('BMW iX Lego','Toys'); 
INSERT INTO product ( productName, ProductCategory ) VALUES ('BMW i6','Cars');
INSERT INTO product ( productName, ProductCategory ) VALUES ('Truck 500c','Vehicle');
INSERT INTO product ( productName, ProductCategory ) VALUES ('Paper Towel','Cleaning Supplies'); ----? Changed from Beauty to Cleaning Supplies 
INSERT INTO product ( productName, ProductCategory ) VALUES ('Plum','Grocery'); 
INSERT INTO product ( productName, ProductCategory ) VALUES ('Lamborghini Lego','Toys'); 
INSERT INTO product ( productName, ProductCategory ) VALUES ('Chicken','Grocery'); 
INSERT INTO product ( productName, ProductCategory ) VALUES ('Pasta','Grocery'); 
INSERT INTO product ( productName, ProductCategory ) VALUES ('PS5','Electronics'); 
INSERT INTO product ( productName, ProductCategory ) VALUES ('Tomato','Grocery'); 
INSERT INTO product ( productName, ProductCategory ) VALUES ('Train X745','Toys');



CREATE TABLE Stores (
    StoreId   NUMBER(4) GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    StoreName VARCHAR2(50) NOT NULL
);
CREATE OR REPLACE TRIGGER track_changes_on_store_table
    AFTER INSERT OR DELETE OR UPDATE ON STORES
        FOR EACH ROW
        DECLARE
            actions Varchar2(20);
        BEGIN
            IF INSERTING THEN
                actions := 'Insert';
                INSERT INTO AuditLog(action,action_time,table_modified,id) VALUES (actions,SYSDATE,'STORES',:NEW.STOREID);
            ELSIF UPDATING THEN
                actions := 'Update';
                INSERT INTO AuditLog(action,action_time,table_modified,id) VALUES (actions,SYSDATE,'STORES',:NEW.STOREID);
            ELSIF DELETING THEN
                actions := 'Delete';
                INSERT INTO AuditLog(action,action_time,table_modified,id) VALUES (actions,SYSDATE,'STORES',:OLD.STOREID);
            END IF;
        END;
/

INSERT INTO stores ( storeName ) VALUES ('Marche Adonis');
INSERT INTO stores ( storeName ) VALUES ('Marche Atwater');
INSERT INTO stores ( storeName ) VALUES ('Dawson Store');
INSERT INTO stores ( storeName ) VALUES ('Store Magic');
INSERT INTO stores ( storeName ) VALUES ('Movie Store');
INSERT INTO stores( storeName ) VALUES ('Super Rue Champlain');
INSERT INTO stores ( storeName ) VALUES ('Toys R Us');
INSERT INTO stores ( storeName ) VALUES ('Dealer One');
INSERT INTO stores ( storeName ) VALUES ('Dealer Montreal');
INSERT INTO stores ( storeName ) VALUES ('Movie Start');
INSERT INTO stores ( storeName ) VALUES ('Star Store');


CREATE TABLE Stores_Product(
    StoreId      NUMBER(4)  REFERENCES Stores(StoreId),
    WarehouseId  NUMBER(4) REFERENCES Warehouse(WarehouseId),
    ProductId    NUMBER(4) REFERENCES Product(ProductId),
    Price        NUMBER(8,2)   NOT NULL
);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (1, 1, 1, 970);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (2, 2, 2, 10);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (3, 3, 3, 50);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (5, 3, 3, 16);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (4, 4, 4, 2);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (5, 5, 5, 30);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (7, 5, 5, 45);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (6, 6, 6, 30);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (6, 6, 6, 10);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (7, 1, 7, 38);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (7, 1, 7, 40);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (8, 1, 8, 50000);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (9, 5, 9, 856600);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (10, 2, 10, 50);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (2, 3, 11, 10);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (7, 5, 12, 80);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (7, 5, 12, 40);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (1, 6, 13, 9.5);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (2, 1, 14, 13.5);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (4, 1, 14, 15);
INSERT INTO Stores_Product (StoreId, WarehouseId, ProductId, Price) VALUES (11, 4, 15, 200);



CREATE TABLE Availability_Warehouse_Product (
    ProductId     NUMBER(4) REFERENCES Product(ProductId) ,
    WarehouseId   NUMBER(4) REFERENCES Warehouse(WarehouseId), 
    Warehouse_qty NUMBER(6) 
);

INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (1, 1, 1000);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (2, 2, 24980);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (3, 3, 103);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (4, 4, 35405);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (5, 5, 40);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (6, 6, 450);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (7, 1, 10);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (8, 1, 6);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (9, 5, 1000);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (10, 6, 3532);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (11, 3, 43242);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (10, 2, 39484);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (11, 4, 6579);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (12, 5, 98765);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (13, 6, 43523);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (14, 1, 2132);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (15, 4, 123);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (16, 1, 352222);
INSERT INTO Availability_Warehouse_Product (ProductId, WarehouseId, Warehouse_qty) VALUES (17, 5, 4543);



CREATE TABLE Customer (
    CustomerId NUMBER(4)    GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    Firstname  VARCHAR2(50) NOT NULL,
    Lastname   VARCHAR2(50) NOT NULL,
    Email      VARCHAR(50)  UNIQUE NOT NULL,
    AddressId  NUMBER(4)    REFERENCES Address(AddressId)
);

CREATE OR REPLACE TYPE CUSTOMER_TYP AS OBJECT(
  CustomerId NUMBER,
    Firstname  VARCHAR2(50),
    Lastname   VARCHAR2(50),
    Email      VARCHAR(50),
    AddressId  NUMBER
);
/

 CREATE OR REPLACE PROCEDURE add_customer (newCustomer IN customer_typ)
IS
BEGIN
    INSERT INTO Customer (Firstname, Lastname, Email, AddressId) VALUES (
        newCustomer.Firstname,
        newCustomer.Lastname,
        newCustomer.Email,
        newCustomer.AddressId);
END add_customer;
/
CREATE OR REPLACE TRIGGER track_changes_on_customer_table
    AFTER INSERT OR DELETE OR UPDATE ON CUSTOMER
        FOR EACH ROW
        DECLARE
            actions Varchar2(20);
        BEGIN
            IF INSERTING THEN
                actions := 'Insert';
                INSERT INTO AuditLog(action,action_time,table_modified,id) VALUES (actions,SYSDATE,'CUSTOMER',:NEW.CUSTOMERID);
            ELSIF UPDATING THEN
                actions := 'Update';
                INSERT INTO AuditLog(action,action_time,table_modified,id) VALUES (actions,SYSDATE,'CUSTOMER',:NEW.CUSTOMERID);
            ELSIF DELETING THEN
                actions := 'Delete';
                INSERT INTO AuditLog(action,action_time,table_modified,id) VALUES (actions,SYSDATE,'CUSTOMER',:OLD.CUSTOMERID);
            END IF;
        END;
/

INSERT INTO Customer ( Firstname, Lastname, Email, AddressId ) VALUES('Daneil', 'Hanne', 'daneil@yahoo.com', (SELECT addressid from Address where street='100 Atwater Street'));
INSERT INTO Customer ( Firstname, Lastname, Email, AddressId ) VALUES('John', 'Boura', 'bdoura@gmail.com', (SELECT addressid from Address where street='100 Young Street'));
INSERT INTO Customer ( Firstname, Lastname, Email, AddressId ) VALUES('Ari', 'Brown','b.a@gmail.com', NULL);
INSERT INTO Customer ( Firstname, Lastname, Email, AddressId ) VALUES('Amanda','Harry','am.harry@yahioo.com',(SELECT addressid from Address where street='100 Saint Laurent'));
INSERT INTO Customer ( Firstname, Lastname, Email, AddressId ) VALUES('Jack','Jonhson','johnson.a@gmail.com',(SELECT addressid from Address a INNER JOIN city c ON a.cityid = c.cityid where a.street IS NULL AND c.city='Calgary' ));
INSERT INTO Customer ( Firstname, Lastname, Email, AddressId ) VALUES('Martin','Alexandre','marting@yahoo.com',(SELECT addressid from Address a INNER JOIN city c ON a.cityid = c.cityid where a.street IS NULL AND c.city='Brossard' ));
INSERT INTO Customer ( Firstname, Lastname, Email, AddressId ) VALUES('Mahsa','Sadeghi','ms@gmail.com',(SELECT addressid from Address where street='104 Gill Street'));
INSERT INTO Customer ( Firstname, Lastname, Email, AddressId ) VALUES('John','Belle','abcd@yahoo.com',(SELECT addressid from Address where street='105 Young Street'));
INSERT INTO Customer ( Firstname, Lastname, Email, AddressId ) VALUES('Alex','Brown','alex@gmail.com',(SELECT addressid from Address where street='boul Saint Laurent'));
INSERT INTO Customer ( Firstname, Lastname, Email, AddressId ) VALUES('Martin','Li','m.li@gmail.com',(SELECT addressid from Address where street='87 boul Saint Laurent'));
INSERT INTO Customer ( Firstname, Lastname, Email, AddressId ) VALUES('Olivia','Smith','smith@hotmail.com',(SELECT addressid from Address where street='76 boul Decalthon'));
INSERT INTO Customer ( Firstname, Lastname, Email, AddressId ) VALUES('Noah','Garcia','g.noah@yahoo.com',(SELECT addressid from Address where street='22222 Happy Street'));
INSERT INTO Customer ( Firstname, Lastname, Email, AddressId ) VALUES('Mahsa','sadeghi','msadeghi@dawsoncollege.qc.ca',(SELECT addressid from Address where street='Dawson College'));

CREATE TABLE Review (
    ReviewId    NUMBER(4) GENERATED ALWAYS AS IDENTITY,
    ProductId   NUMBER(4) REFERENCES Product(ProductId),
    CustomerId  NUMBER(4) REFERENCES Customer(CustomerId),
    Rating      NUMBER(1) ,
    Review_flag NUMBER(1) ,
    productDescription VARCHAR2(200),
    CONSTRAINT CHK_review CHECK (Review_flag<=2 AND Review_flag>=0)
);
CREATE OR REPLACE TYPE REVIEW_TYP AS OBJECT(
    ReviewId    NUMBER,
    ProductId   NUMBER,
    CustomerId  NUMBER,
    Rating      NUMBER,
    Review_flag NUMBER,
    productDescription VARCHAR2(200)
);
/
CREATE OR REPLACE TRIGGER track_review_changes
    AFTER INSERT ON Review
    FOR EACH ROW
    DECLARE
        action VARCHAR2(20);
    BEGIN
        IF INSERTING THEN
            action := 'Insert';
        END IF;

        INSERT INTO AuditLog (action, action_time, table_modified, id)
            VALUES (action, SYSDATE, 'Review', :NEW.REVIEWID);
    END;
/
CREATE OR REPLACE PROCEDURE add_review (newReview IN REVIEW_TYP)
IS
BEGIN
    INSERT INTO Review (ProductId, CustomerId, Rating, Review_flag, productDescription)
    VALUES (newReview.ProductId, newReview.CustomerId, newReview.Rating, newReview.Review_flag, newReview.productDescription);
END add_review;

/

INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES 
((SELECT productId FROM PRODUCT WHERE productName='Laptop ASUS 104S'),
(SELECT customerId FROM CUSTOMER WHERE Email='msadeghi@dawsoncollege.qc.ca'),4,0,'It was affordable.');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES
((SELECT productId FROM PRODUCT WHERE productName='Apple'),
(SELECT customerId FROM CUSTOMER WHERE Email='alex@gmail.com'),3,0,'Quality was not good');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES
((SELECT productId FROM PRODUCT WHERE productName='Sims Cd'),
(SELECT customerId FROM CUSTOMER WHERE Email='marting@yahoo.com'),2,1,'');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES 
((SELECT productId FROM PRODUCT WHERE productName='Orange'),
(SELECT customerId FROM CUSTOMER WHERE Email='daneil@yahoo.com'),5,0,'Highly recommend');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES 
((SELECT productId FROM PRODUCT WHERE productName='Barbie Movie'),
(SELECT customerId FROM CUSTOMER WHERE Email='alex@gmail.com'),1,0,'');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES 
((SELECT productId FROM PRODUCT WHERE productName='L''Oreal Normal Hair'),
(SELECT customerId FROM CUSTOMER WHERE Email='marting@yahoo.com'),1,0,'Did not worth the price');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES
((SELECT productId FROM PRODUCT WHERE productName='BMW iX Lego'),
(SELECT customerId FROM CUSTOMER WHERE Email='msadeghi@dawsoncollege.qc.ca'),1,0,'missing some parts');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES
((SELECT productId FROM PRODUCT WHERE productName='BMW i6'),
(SELECT customerId FROM CUSTOMER WHERE Email='bdoura@gmail.com'),5,1,'Trash');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES
((SELECT productId FROM PRODUCT WHERE productName='Truck 500c'),
(SELECT customerId FROM CUSTOMER WHERE Email='b.a@gmail.com'),2,NULL,'');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES 
((SELECT productId FROM PRODUCT WHERE productName='Paper Towel'),
(SELECT customerId FROM CUSTOMER WHERE Email='am.harry@yahioo.com'),5,NULL,'');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES
((SELECT productId FROM PRODUCT WHERE productName='Plum'),
(SELECT customerId FROM CUSTOMER WHERE Email='johnson.a@gmail.com'),4,NULL,'');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES 
((SELECT productId FROM PRODUCT WHERE productName='L''Oreal Normal Hair'),
(SELECT customerId FROM CUSTOMER WHERE Email='marting@yahoo.com'),3,NULL,'');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES 
((SELECT productId FROM PRODUCT WHERE productName='Lamborghini Lego'),
(SELECT customerId FROM CUSTOMER WHERE Email='msadeghi@dawsoncollege.qc.ca'),1,0,'Missing some parts');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES 
((SELECT productId FROM PRODUCT WHERE productName='Plum'),
(SELECT customerId FROM CUSTOMER WHERE Email='msadeghi@dawsoncollege.qc.ca'),4,NULL,'');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES
((SELECT productId FROM PRODUCT WHERE productName='Lamborghini Lego'),
(SELECT customerId FROM CUSTOMER WHERE Email='ms@gmail.com'),1,0,'Great Product');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES 
((SELECT productId FROM PRODUCT WHERE productName='BMW i6'),
(SELECT customerId FROM CUSTOMER WHERE Email='abcd@yahoo.com'),5,1,'Bad Quality');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES 
((SELECT productId FROM PRODUCT WHERE productName='Sims Cd'),
(SELECT customerId FROM CUSTOMER WHERE Email='alex@gmail.com'),1,0,'');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES 
((SELECT productId FROM PRODUCT WHERE productName='Barbie Movie'),
(SELECT customerId FROM CUSTOMER WHERE Email='alex@gmail.com'),4,0,'');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES 
((SELECT productId FROM PRODUCT WHERE productName='Chicken'),
(SELECT customerId FROM CUSTOMER WHERE Email='m.li@gmail.com'),4,NULL,'');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES 
((SELECT productId FROM PRODUCT WHERE productName='Pasta'),
(SELECT customerId FROM CUSTOMER WHERE Email='smith@hotmail.com'),5,NULL,'');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES
((SELECT productId FROM PRODUCT WHERE productName='PS5'),
(SELECT customerId FROM CUSTOMER WHERE Email='g.noah@yahoo.com'),NULL,NULL,'');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES 
((SELECT productId FROM PRODUCT WHERE productName='BMW iX Lego'),
(SELECT customerId FROM CUSTOMER WHERE Email='msadeghi@dawsoncollege.qc.ca'),1,2,'worse car i have droven!');
INSERT INTO review ( productId,customerId,rating,review_flag,productDescription ) VALUES 
((SELECT productId FROM PRODUCT WHERE productName='Pasta'),
(SELECT customerId FROM CUSTOMER WHERE Email='smith@hotmail.com'),4,NULL,'');



CREATE TABLE Orders(
    OrderId     NUMBER(4) GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    CustomerId  NUMBER(4) REFERENCES Customer(CustomerId),
    ProductId   NUMBER(4) REFERENCES Product(ProductId),
    OrderDate   DATE ,
    StoreId     NUMBER(4)  REFERENCES Stores (StoreId)

);
CREATE OR REPLACE TRIGGER track_changes_on_order_table
    AFTER INSERT OR DELETE OR UPDATE ON ORDERS
        FOR EACH ROW
        DECLARE
            actions Varchar2(20);
        BEGIN
            IF INSERTING THEN
                actions := 'Insert';
                INSERT INTO AuditLog(action,action_time,table_modified,id) VALUES (actions,SYSDATE,'ORDERS',:NEW.ORDERID);
            ELSIF UPDATING THEN
                actions := 'Update';
                INSERT INTO AuditLog(action,action_time,table_modified,id) VALUES (actions,SYSDATE,'ORDERS',:NEW.ORDERID);
            ELSIF DELETING THEN
                actions := 'Delete';
                INSERT INTO AuditLog(action,action_time,table_modified,id) VALUES (actions,SYSDATE,'ORDERS',:OLD.ORDERID);
            END IF;
        END;
/
 CREATE OR REPLACE PROCEDURE add_order (newOrder IN order_typ)
IS
    v_order_id NUMBER;
BEGIN
    INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId)
    VALUES (newOrder.CustomerId, newOrder.ProductId, newOrder.OrderDate, newOrder.StoreId)
    RETURNING OrderId INTO v_order_id;
    
    INSERT INTO Orders_Product (OrderId, ProductId, product_qty)
    VALUES (v_order_id, newOrder.ProductId, newOrder.ProductQty);
END add_order;

/

INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (13, 1, TO_DATE('2023-04-21', 'YYYY-MM-DD'), 1);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (9, 2, TO_DATE('2023-10-23', 'YYYY-MM-DD'), 2);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (6, 3, TO_DATE('2023-10-01', 'YYYY-MM-DD'), 3);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (1, 4, TO_DATE('2023-10-23', 'YYYY-MM-DD'), 4);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (9, 5, TO_DATE('2023-10-23', 'YYYY-MM-DD'), 5);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (6, 6, TO_DATE('2023-10-10', 'YYYY-MM-DD'), 6);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (13, 7, TO_DATE('2023-10-11', 'YYYY-MM-DD'), 7);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (2, 8, TO_DATE('2023-10-10', 'YYYY-MM-DD'), 8);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (3, 9, NULL, 9);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (4, 10, NULL, 10);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (5, 11, TO_DATE('2020-05-06', 'YYYY-MM-DD'), 2);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (6, 6, TO_DATE('2019-09-12', 'YYYY-MM-DD'), 6);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (13, 12, TO_DATE('2010-10-11', 'YYYY-MM-DD'), 7);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (13, 11, TO_DATE('2022-05-06', 'YYYY-MM-DD'), 2);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (7, 12, TO_DATE('2023-10-07', 'YYYY-MM-DD'), 7);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (8, 8, TO_DATE('2023-08-10', 'YYYY-MM-DD'), 8);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (9, 3, TO_DATE('2023-10-23', 'YYYY-MM-DD'), 5);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (9, 5, TO_DATE('2023-10-02', 'YYYY-MM-DD'), 7);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (10, 13, TO_DATE('2019-04-03', 'YYYY-MM-DD'), 1);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (11, 14, TO_DATE('2021-12-29', 'YYYY-MM-DD'), 2);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (12, 15, TO_DATE('2020-01-20', 'YYYY-MM-DD'), 11);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (13, 7, TO_DATE('2022-10-11', 'YYYY-MM-DD'), 7);
INSERT INTO Orders (CustomerId, ProductId, OrderDate, StoreId) VALUES (11, 14, TO_DATE('2021-12-29', 'YYYY-MM-DD'), 4);


CREATE TABLE Orders_Product(
    OrderId     NUMBER(4)  REFERENCES Orders (OrderId),
    ProductId   NUMBER(4) REFERENCES Product(ProductId),
    product_qty NUMBER(3) NOT NULL
    );
    
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (1, 1, 1);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (2, 2, 2);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (3, 3, 3);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (4, 4, 1);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (5, 5, 1);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (6, 6, 1);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (7, 7, 1);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (8, 8, 1);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (9, 9, 1);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (10, 10, 3);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (11, 11, 6);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (12, 6, 3);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (13, 12, 1);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (14, 11, 7);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (15, 12, 2);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (16, 8, 1);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (17, 3, 1);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (18, 5, 1);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (19, 13, 1);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (20, 14, 3);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (21, 15, 1);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (22, 7, 1);
INSERT INTO Orders_Product (OrderId, ProductId, product_qty) VALUES (23, 14, 3);

--Create objects for Java

CREATE OR REPLACE TYPE PROVINCE_TYP AS OBJECT(
    ProvinceId NUMBER(4),
    Province   VARCHAR2(50)
);
/

CREATE OR REPLACE TYPE CITY_TYP AS OBJECT(
    CityId     NUMBER(4),
    City       VARCHAR2(50),
    ProvinceId NUMBER(4)
);
/

CREATE OR REPLACE TYPE ADDRESS_TYP AS OBJECT(
    AddressId NUMBER(4),
    Street    VARCHAR2(50),
    CityId    NUMBER(4),
    Country   VARCHAR2(50)
);
/

CREATE OR REPLACE TYPE WAREHOUSE_TYP AS OBJECT(
    WarehouseId   NUMBER(4),
    WarehouseName VARCHAR2(50),
    AddressId     NUMBER(4)
);
/

CREATE OR REPLACE TYPE STORE_TYP AS OBJECT(
    StoreId   NUMBER(4),
    StoreName VARCHAR2(50)
);
/

CREATE OR REPLACE TYPE STORES_PRODUCT_TYP AS OBJECT(
    StoreId      NUMBER(4),
    WarehouseId  NUMBER(4),
    ProductId    NUMBER(4),
    Price        NUMBER(8,2)
);
/

CREATE OR REPLACE TYPE AVAILABILITY_WAREHOUSE_PRODUCT_TYP AS OBJECT(
    ProductId     NUMBER(4),
    WarehouseId   NUMBER(4),
    Warehouse_qty NUMBER(6)
);
/


CREATE OR REPLACE TYPE order_typ AS OBJECT (
    CustomerId NUMBER,
    ProductId NUMBER,
    OrderDate DATE,
    StoreId NUMBER,
    ProductQty NUMBER
);
/

CREATE OR REPLACE TYPE ORDERS_PRODUCT_TYP AS OBJECT(
    OrderId     NUMBER(4),
    ProductId   NUMBER(4),
    product_qty NUMBER(3)
);
/
CREATE OR REPLACE TYPE order_detail_type FORCE AS OBJECT (
    order_id NUMBER,
    order_date DATE,
    product_name VARCHAR2(100),
    product_id NUMBER
);
/

CREATE OR REPLACE TYPE order_detail_varray AS VARRAY(100) OF order_detail_type;
/

CREATE OR REPLACE TYPE product_detail_type FORCE AS OBJECT (
    ProductId NUMBER,
    ProductName VARCHAR2(30),
    Price NUMBER(8,2),
    ReviewDescription VARCHAR2(200)
);
/

CREATE OR REPLACE TYPE product_detail_varray AS VARRAY(100) OF product_detail_type;
/
CREATE OR REPLACE TYPE product_detail_type_store FORCE AS OBJECT (
    ProductId NUMBER(4),
    ProductName VARCHAR2(30),
    Price NUMBER(8,2)
);
/
CREATE OR REPLACE TYPE product_detail_varray_store AS VARRAY(100) OF product_detail_type_store;
/
CREATE OR REPLACE TYPE store_detail_type FORCE AS OBJECT (
    StoreId NUMBER(4),
    StoreName VARCHAR2(50)
);
/

CREATE OR REPLACE TYPE store_detail_array AS VARRAY(100) OF store_detail_type;
/
CREATE OR REPLACE TYPE Warehouse_Details FORCE AS OBJECT (
    WarehouseId NUMBER,
    WarehouseName VARCHAR2(50)
);
/

CREATE OR REPLACE TYPE Warehouse_Details_Array AS VARRAY(100) OF Warehouse_Details;
/
CREATE OR REPLACE TYPE Product_Details AS OBJECT (
    ProductId NUMBER,
    ProductName VARCHAR2(100)
);
/
CREATE OR REPLACE TYPE Product_Details_Array_all AS VARRAY(100) OF Product_Details;
/

CREATE OR REPLACE TYPE Order_Details FORCE AS OBJECT (
    OrderId NUMBER,
    OrderDate DATE,
    CustomerId NUMBER,
    StoreId NUMBER
);
/

CREATE OR REPLACE TYPE Order_Details_Array AS VARRAY(1000) OF Order_Details;
/
CREATE OR REPLACE TYPE category_detail_type FORCE AS OBJECT (
    CategoryName VARCHAR2(200)
);
/


CREATE OR REPLACE TYPE category_detail_varray AS VARRAY(100) OF category_detail_type;
/
CREATE OR REPLACE TYPE ReviewList AS VARRAY(100) OF REVIEW_TYP;
/

