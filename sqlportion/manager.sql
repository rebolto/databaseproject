DROP PACKAGE managerSide;

CREATE OR REPLACE PACKAGE managerSide AS

    TYPE DisplayQty IS REF CURSOR;
    PROCEDURE DisplayAuditLog (
        p_AuditLogRecords OUT SYS_REFCURSOR
    );
    FUNCTION DisplayQtyOfProduct(
    w_warehouseId in Warehouse.warehouseId%type
    )return DisplayQty;
    PROCEDURE DeleteProduct (
        p_ProductId IN Product.ProductId%TYPE
    );
    PROCEDURE DeleteWarehouse(
    w_WarehouseId in Warehouse.WarehouseId%TYPE
    ); 
    FUNCTION GetProductList RETURN Product_Details_Array_all;
    FUNCTION get_all_stores RETURN store_detail_array;
    FUNCTION get_products_by_store(
        p_storeId NUMBER
    ) RETURN product_detail_varray_store;
    PROCEDURE UpdateProductPrice (
        p_StoreId IN Stores_Product.StoreId%TYPE,
        p_ProductId IN Stores_Product.ProductId%TYPE,
        p_NewPrice IN Stores_Product.Price%TYPE
    );
    

    FUNCTION GetAllWarehouses RETURN Warehouse_Details_Array;

    PROCEDURE UpdateProductQuantityInWarehouse (
        p_ProductId IN Availability_Warehouse_Product.ProductId%TYPE,
        p_WarehouseId IN Availability_Warehouse_Product.WarehouseId%TYPE,
        p_NewQuantity IN Availability_Warehouse_Product.Warehouse_qty%TYPE
    );
   
END managerSide;
/

CREATE OR REPLACE PACKAGE BODY managerSide AS

--delete product given productid
 PROCEDURE DeleteProduct (
    p_ProductId IN Product.ProductId%TYPE
) AS
BEGIN
    DELETE FROM Product
    WHERE ProductId = p_ProductId;

    COMMIT;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        -- No row was found to delete, which can be handled silently or with an error
        RAISE_APPLICATION_ERROR(-20003, 'No product found with the specified ID.');
    WHEN OTHERS THEN
        ROLLBACK;
        RAISE;
END DeleteProduct;

 -- Delete warehouse given warehouseid
 PROCEDURE DeleteWarehouse(
    w_WarehouseId in Warehouse.WarehouseId%TYPE
 ) AS
 BEGIN
    Delete FROM Warehouse
    WHERE WarehouseId=w_Warehouseid;
    COMMIT;
 EXCEPTION
    WHEN NO_DATA_FOUND THEN
    RAISE_APPLICATION_ERROR(-20006,'No warehouse found with the specified ID.');
    WHEN OTHERS THEN
    ROLLBACK;
    RAISE;
 
 End DeleteWarehouse;

--displays quantity of a product given warehouseid
FUNCTION DisplayQtyOfProduct(
    w_warehouseID IN Warehouse.warehouseId%type
) RETURN DisplayQty IS
    v_displayQty DisplayQty;
BEGIN
    OPEN v_displayQty FOR
        SELECT awp.ProductId, awp.warehouse_qty
        FROM Warehouse w join Availability_Warehouse_Product awp on awp.warehouseId=w.warehouseId 
        WHERE w.WarehouseId = w_warehouseID;

    RETURN v_displayQty;
END DisplayQtyOfProduct;

--displays auditlog
 PROCEDURE DisplayAuditLog (
        p_AuditLogRecords OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN p_AuditLogRecords FOR
            SELECT *
            FROM auditLog;
    END DisplayAuditLog;
    
--return all productid productname 
FUNCTION GetProductList RETURN Product_Details_Array_all AS
    v_products Product_Details_Array_all := Product_Details_Array_all();
    v_counter NUMBER := 1;
BEGIN
    FOR rec IN (SELECT ProductId, ProductName FROM Product) LOOP
        v_products.EXTEND;
        v_products(v_counter) := Product_Details(rec.ProductId, rec.ProductName);
        v_counter := v_counter + 1;
    END LOOP;

    RETURN v_products;
END GetProductList;


-- prints all the stores and their ids 
FUNCTION get_all_stores
    RETURN store_detail_array IS

    stores store_detail_array := store_detail_array();
    
BEGIN
    FOR rec IN (SELECT StoreId, StoreName FROM Stores) LOOP
        stores.EXTEND;
        stores(stores.LAST) := store_detail_type(rec.StoreId, rec.StoreName);
    END LOOP;

    RETURN stores;
END get_all_stores;

--prints all products from a given store
FUNCTION get_products_by_store(p_storeId NUMBER)
    RETURN product_detail_varray_store IS

    product_list product_detail_varray_store := product_detail_varray_store();
    
BEGIN
    FOR rec IN (
        SELECT p.ProductId, p.ProductName, sp.Price
        FROM Product p
        JOIN Stores_Product sp ON p.ProductId = sp.ProductId
        WHERE sp.StoreId = p_storeId
    ) LOOP
        product_list.EXTEND;
        product_list(product_list.LAST) := product_detail_type_store(rec.ProductId, rec.ProductName, rec.Price);
    END LOOP;

    RETURN product_list;
END get_products_by_store;

--update price of product
PROCEDURE UpdateProductPrice (
    p_StoreId IN Stores_Product.StoreId%TYPE,
    p_ProductId IN Stores_Product.ProductId%TYPE,
    p_NewPrice IN Stores_Product.Price%TYPE
) AS
BEGIN
    UPDATE Stores_Product
    SET Price = p_NewPrice
    WHERE StoreId = p_StoreId AND ProductId = p_ProductId;

    COMMIT;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        ROLLBACK;
        RAISE_APPLICATION_ERROR(-20001, 'No product found with the specified ID in the given store.');
    WHEN OTHERS THEN
        ROLLBACK;
        RAISE;
END UpdateProductPrice;



-- all the warehouses and their ids 

FUNCTION GetAllWarehouses RETURN Warehouse_Details_Array AS
    v_warehouses Warehouse_Details_Array := Warehouse_Details_Array();
    v_counter NUMBER := 1;
BEGIN
    FOR rec IN (SELECT WarehouseId, WarehouseName FROM Warehouse) LOOP
        v_warehouses.EXTEND;
        v_warehouses(v_counter) := Warehouse_Details(rec.WarehouseId, rec.WarehouseName);
        v_counter := v_counter + 1;
    END LOOP;

    RETURN v_warehouses;
END GetAllWarehouses;

--update quantity of product in warehouse given name and quantity
 PROCEDURE UpdateProductQuantityInWarehouse (
    p_ProductId IN Availability_Warehouse_Product.ProductId%TYPE,
    p_WarehouseId IN Availability_Warehouse_Product.WarehouseId%TYPE,
    p_NewQuantity IN Availability_Warehouse_Product.Warehouse_qty%TYPE
) AS
BEGIN
    UPDATE Availability_Warehouse_Product
    SET Warehouse_qty = p_NewQuantity
    WHERE ProductId = p_ProductId AND WarehouseId = p_WarehouseId;

    COMMIT;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        ROLLBACK;
        RAISE_APPLICATION_ERROR(-20005, 'No product found with the specified ID in the given warehouse.');
    WHEN OTHERS THEN
        ROLLBACK;
        RAISE;
END UpdateProductQuantityInWarehouse;

    END managerSide;
