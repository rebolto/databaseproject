DROP PACKAGE customerSide;

CREATE OR REPLACE PACKAGE customerSide AS

    PROCEDURE GetCustomerOrders(p_email IN VARCHAR2, o_orders OUT order_detail_varray);
    FUNCTION GetCustomerIdByEmail(p_Email IN Customer.Email%TYPE)
    RETURN Customer.CustomerId%TYPE;
    FUNCTION get_all_categories RETURN category_detail_varray;
    FUNCTION get_products_by_category(p_category VARCHAR2)
    RETURN product_detail_varray;
    FUNCTION get_store_id_by_product(p_product_id NUMBER) RETURN NUMBER;
    PROCEDURE UpdateCustomerEmail (
            p_CustomerId IN Customer.CustomerId%TYPE,
            p_NewEmail   IN Customer.Email%TYPE
    );
    FUNCTION  GetAllReviewsByEmail(p_Email IN Customer.Email%TYPE) RETURN ReviewList;
    PROCEDURE UpdateReviewDescription(
        p_ReviewId IN REVIEW.ReviewId%TYPE,
        p_productDescription IN REVIEW.ProductDescription%TYPE
    );
    PROCEDURE DeleteReview(
        p_ReviewId IN REVIEW.ReviewId%TYPE
    );
    PROCEDURE DeleteOrder(
        p_OrderId IN ORDERS.OrderId%TYPE
    );
END customerSide;

CREATE OR REPLACE PACKAGE BODY customerSide AS

--return customer orders including order date and product, from their email. 
--the return type is array and can be stored  in java. Then you can loop through that array and print it in java
--will be callable statement 
PROCEDURE GetCustomerOrders(
    p_email IN VARCHAR2, 
    o_orders OUT order_detail_varray
) AS
    v_orders order_detail_varray := order_detail_varray();
    v_index NUMBER := 1;
BEGIN
    FOR rec IN (
        SELECT o.OrderId, o.OrderDate, p.ProductName, p.ProductId
        FROM Orders o
        JOIN Customer c ON o.CustomerId = c.CustomerId
        JOIN Orders_Product op ON op.OrderId = o.OrderId
        JOIN Product p ON op.ProductId = p.ProductId
        WHERE c.Email = p_email
    )
    LOOP
        v_orders.EXTEND;
        v_orders(v_index) := order_detail_type(rec.OrderId, rec.OrderDate, rec.ProductName, rec.ProductId);
        v_index := v_index + 1;
    END LOOP;

    o_orders := v_orders;
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error occurred: ' || SQLERRM);
END GetCustomerOrders;


--get customerid by email
--will take as input email in java and that will generate the customerid
--of a client with that email 
FUNCTION GetCustomerIdByEmail (
    p_Email IN Customer.Email%TYPE
) RETURN Customer.CustomerId%TYPE AS
    v_CustomerId Customer.CustomerId%TYPE;
BEGIN
    SELECT CustomerId INTO v_CustomerId
    FROM Customer
    WHERE Email = p_Email;

    RETURN v_CustomerId;

EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20001, 'No customer found with that email.');
    WHEN OTHERS THEN
        RAISE;
END GetCustomerIdByEmail;


--returns all categories 
--will store all categories of superStore in an array
--we could then loop throght that array in java ans print all categories
FUNCTION get_all_categories RETURN category_detail_varray AS
    v_categories category_detail_varray := category_detail_varray();
BEGIN
    FOR rec IN (
        SELECT DISTINCT ProductCategory AS CategoryName
        FROM Product
    )
    LOOP
        v_categories.EXTEND;
        v_categories(v_categories.LAST) := category_detail_type(rec.CategoryName);
    END LOOP;

    RETURN v_categories;
END get_all_categories;


-- get products in a given category
--the funtion takes a category as parameter and returns an array of all the products in that category
FUNCTION get_products_by_category(p_category VARCHAR2)
    RETURN product_detail_varray IS

    product_details product_detail_varray := product_detail_varray();
    
BEGIN
    FOR rec IN (
        SELECT p.ProductId, p.ProductName, sp.Price, r.ProductDescription AS ReviewDescription
        FROM Product p
        JOIN Stores_Product sp ON p.ProductId = sp.ProductId
        JOIN Review r ON p.ProductId = r.ProductId
        WHERE p.ProductCategory = p_category
    )
    LOOP
        product_details.EXTEND;
        product_details(product_details.LAST) := product_detail_type(rec.ProductId, rec.ProductName, rec.Price, rec.ReviewDescription);
    END LOOP;

    RETURN product_details;
END get_products_by_category;


--the funtion returns the storeid given a productid
 FUNCTION get_store_id_by_product(p_product_id NUMBER)
RETURN NUMBER IS
    v_store_id Stores.StoreId%TYPE;
BEGIN
    SELECT MIN(StoreId) INTO v_store_id
    FROM Stores_Product
    WHERE ProductId = p_product_id;

    RETURN v_store_id;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN NULL;
END get_store_id_by_product;


--the procedure takes as parameter the customerid and a new email,
--then proceeds to change the email of the customer with the provided customerid
PROCEDURE UpdateCustomerEmail (
    p_CustomerId IN Customer.CustomerId%TYPE,
    p_NewEmail   IN Customer.Email%TYPE
) AS
BEGIN
    UPDATE Customer
    SET Email = p_NewEmail
    WHERE CustomerId = p_CustomerId;
    
EXCEPTION
    WHEN OTHERS THEN
 DBMS_OUTPUT.PUT_LINE('Error occurred: ' || SQLERRM);
        ROLLBACK;
        RAISE;
END UpdateCustomerEmail;


--the funtion returns an array of reviews given a customer
--it will take as parameter the customerid and return all the reviews
--made by the customer with that customerid
FUNCTION GetAllReviewsByEmail(p_Email IN Customer.Email%TYPE) RETURN ReviewList IS
    v_reviews ReviewList := ReviewList();
BEGIN
    SELECT CAST(MULTISET(
        SELECT REVIEW_TYP(ReviewId, ProductId, r.CustomerId, Rating, Review_flag, PRODUCTDESCRIPTION)
        FROM Review r
        JOIN Customer c ON r.CustomerId = c.CustomerId
        WHERE c.Email = p_Email
    ) AS ReviewList) INTO v_reviews FROM DUAL;

    RETURN v_reviews;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN v_reviews;
    WHEN OTHERS THEN
        RAISE;
END GetAllReviewsByEmail;
  
  --updates the description of a review 
 PROCEDURE UpdateReviewDescription (
    p_ReviewId IN REVIEW.ReviewId%TYPE,
    p_ProductDescription IN REVIEW.ProductDescription%TYPE
) AS
BEGIN
    UPDATE Review
    SET ProductDescription = p_ProductDescription
    WHERE ReviewId = p_ReviewId;
    COMMIT;
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        RAISE;
END UpdateReviewDescription;


--deletes review given reviewid
PROCEDURE DeleteReview (
    p_ReviewId IN REVIEW.ReviewId%TYPE
) AS
BEGIN
    DELETE FROM Review
    WHERE ReviewId = p_ReviewId;

    COMMIT;
EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Error occurred: ' || SQLERRM);
        ROLLBACK;
        RAISE;
END DeleteReview;


--deletes an order given orderid
PROCEDURE DeleteOrder (
    p_OrderId IN ORDERS.OrderId%TYPE
) AS
BEGIN
    DELETE FROM Orders_Product
    WHERE OrderId = p_OrderId;

    DELETE FROM Orders
    WHERE OrderId = p_OrderId;

    COMMIT;
EXCEPTION
    WHEN OTHERS THEN
 DBMS_OUTPUT.PUT_LINE('Error occurred: ' || SQLERRM);
        ROLLBACK;
        RAISE;
END DeleteOrder;


END customerSide;

/





 

