# SuperStore Management System

## Overview
The SuperStore Management System is a Java-based application connected to an Oracle SQL database designed to simulate a retail management system. This project creates an interactive platform where customers and managers can perform various operations. Customers can place orders, write reviews, and manage their information, while managers can track and update inventory, manage product pricing, and access audit logs. The system is structured to ensure a clear division of functionalities based on user roles, ensuring an organized and efficient management process.

## Getting Started

### Understanding the Database
Before beginning, familiarize yourself with the database structure by reviewing the Entity-Relationship Diagram (ERD) provided in the project repository. This will give you insight into the database's tables, relationships, and how they interact within the application.

### Initial Setup
1. **Clear Existing Data:** Run the `remove.sql` script to clear any existing data that may interfere with the setup process.
2. **Database Setup:** Execute the `setup.sql` script to create tables, types, triggers, and other database structures.
3. **Prepare Database Operations:** Execute the `customer.sql` and `manager.sql` scripts to load stored procedures and functions necessary for the application's operation.

### Interacting with the Database
With the database set up, you can use the Java application to interact with the system. The application provides a user-friendly command-line interface for various operations.

## Project Structure

### SQL Scripts
Located in the `sqlportion` folder, the scripts are categorized as follows:
- **Removal Script (`remove.sql`):** Cleans up the database to ensure a fresh start.
- **Setup Script (`setup.sql`):** Initializes the database with the required schema and sample data.
- **Packages Scripts (`customer.sql`, `manager.sql`):** Contain stored procedures, functions, and types for different user roles.

### Java Application
The Java application, housed in the `javaportion` folder, includes classes that correspond to database entities and facilitate user interaction with the database.

### Java Classes
- `CompleteAddress.java`
- `Customer.java`
- `Product.java`
- `Orders.java`
- `Review.java`
- `App.java`: The main class that runs the application.
- `CustomerHelper.java`: Contains methods for customer-related operations.
- `ManagerHelper.java`: Contains methods for manager-related operations.
- `StoreListServices.java`: Facilitates communication between the Java application and the SQL database.

## Contributors
- Ivana Zhekova: ERD design, data insertion, CustomerSide package, ManagerSide package, Java application for customers.
- Noah Diplarakis: ManagerSide package, database triggers, Java application for managers.

## Project Status
The project is complete and fully functional.