package javaportion;

import java.sql.*;
import java.util.Map;

/**
 * The CompleteAddress class represents a review entity in the SuperStore
 * Database System.
 */

public class CompleteAddress implements SQLData {
    private String sql_type = "ADDRESS_TYP";
    private String street;
    private String city;
    private String province;
    private String country;
    private int addressId;

    /**
     * Constructs a new CompleteAddress with the specified street, city, province,
     * and country.
     *
     * @param street   the street of the address
     * @param city     the city of the address
     * @param province the province of the address
     * @param country  the country of the address
     */
    
    public CompleteAddress(String street, String city, String province, String country) {
        this.street = street;
        this.city = city;
        this.province = province;
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getAddressId() {
        return addressId;
    }

    @Override
    public String getSQLTypeName() {
        return sql_type;
    }

    /**
     * Reads data from the given SQL stream to populate this CompleteAddress
     * object's fields.
     *
     * @param stream   the SQLInput stream from which this CompleteAddress will be
     *                 read
     * @param typeName the SQL type name of the data being read
     * @throws SQLException if a database access error occurs
     */

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.street = stream.readString();
        this.city = stream.readString();
        this.province = stream.readString();
        this.country = stream.readString();

    }

    /**
     * Writes this CompleteAddress object to a SQL stream, effectively serializing
     * it to be stored in a SQL database.
     *
     * @param stream the SQLOutput stream to which this CompleteAddress will be
     *               written
     * @throws SQLException if a database access error occurs
     */

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(this.street);
        stream.writeString(this.city);
        stream.writeString(this.province);
        stream.writeString(this.country);
       
    }

    /**
     * Adds this CompleteAddress object to the database using the given connection.
     * It maps the object to the SQL structured type and calls the respective
     * stored procedure to add the address to the database.
     *
     * @param conn the Connection object to the database
     * @throws SQLException           if a database access error occurs or this
     *                                method is
     *                                called on a closed connection
     * @throws ClassNotFoundException if the ADDRESS_TYP class is not found in the
     *                                Java context
     */

     public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        CallableStatement cstmt = conn.prepareCall("{call add_complete_address(?, ?, ?, ?, ?)}");
    
        try {
            cstmt.setString(1, this.street);
            cstmt.setString(2, this.city);
            cstmt.setString(3, this.province);
            cstmt.setString(4, this.country);
    
            cstmt.registerOutParameter(5, Types.INTEGER);
    
            cstmt.execute();
    
            this.addressId = cstmt.getInt(5);
        } finally {
            if (cstmt != null) {
                cstmt.close();
            }
        }
    }
    
}
