package javaportion;

import java.sql.*;
import oracle.jdbc.OracleTypes;

/**
 * @author Ivana Zhekova 2038099 & Noah Diplarakis 2244592
 * 
 *         StoreListServices.java
 * 
 *         The StoreListServices class provides various services for interacting
 *         with a SuperStore Database.
 *         It includes functionalities for connecting to the database,
 *         performing multiple operations,
 *         and executing stored procedures to handle different entities such as
 *         customers, orders, reviews, products, and stores.
 * 
 */

public class StoreListServices {

    private Connection connection;

    /**
     * Constructor to create a StoreListServices object and establish a database
     * connection.
     *
     * @param user the username for the database connection
     * @param pwd  the password for the database connection
     */
    public StoreListServices(String user, String pwd) {
        try {
            String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
            this.connection = DriverManager.getConnection(url, user, pwd);
            System.out.println("connected successfully");
        } catch (SQLException e) {
            System.out.println("Error connecting to the database...");
            e.printStackTrace();
        }
    }

    /**
     * Retrieves the active database connection.
     *
     * @return the active Connection object
     */
    public Connection getConnection() {
        return this.connection;
    }

    /**
     * Closes the active database connection.
     */
    public void close() {
        try {
            if (this.connection != null && !this.connection.isClosed()) {
                this.connection.close();
            }
        } catch (SQLException e) {
            System.out.println("Error closing the database connection...");
            e.printStackTrace();
        }
    }

    // CUSTOMER SIDE

    /**
     * Displays all orders associated with the customer's email.
     *
     * @param email the email of the customer
     * @return true if orders were found and displayed, false otherwise
     */

    public boolean displayCustomerOrders(String email) {
        CallableStatement callableStatement = null;
        boolean ordersFound = false;
        try {
            String procedureCall = "{ call customerSide.GetCustomerOrders(?, ?) }";
            callableStatement = this.connection.prepareCall(procedureCall);

            callableStatement.setString(1, email);
            callableStatement.registerOutParameter(2, java.sql.Types.ARRAY, "ORDER_DETAIL_VARRAY");

            callableStatement.execute();

            Array orderDetailsArray = callableStatement.getArray(2);
            Object[] orderDetails = (Object[]) orderDetailsArray.getArray();

            if (orderDetails.length > 0) {
                ordersFound = true;
                for (Object orderDetailObj : orderDetails) {
                    Struct orderDetailStruct = (Struct) orderDetailObj;
                    Object[] orderDetailFields = orderDetailStruct.getAttributes();
                    System.out.println("Order ID: " + orderDetailFields[0]);
                    System.out.println("Order Date: " + orderDetailFields[1]);
                    System.out.println("Product Name: " + orderDetailFields[2]);
                    System.out.println("Product ID: " + orderDetailFields[3]);
                    System.out.println("-----------------------------------");
                }
            } else {
                System.out.println("No orders found for email: " + email);
            }
        } catch (SQLException e) {
            System.out.println("Error occurred: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (callableStatement != null) {
                try {
                    callableStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return ordersFound;
    }

    /**
     * Retrieves the customer ID associated with the given email.
     *
     * @param email the email of the customer
     * @return the customer ID or -1 if the customer does not exist
     */

    public int getCustomerIdByEmail(String email) {
        int customerId = -1;
        String sql = "{ ? = call customerSide.GetCustomerIdByEmail(?) }";

        try (CallableStatement stmt = this.connection.prepareCall(sql)) {
            stmt.registerOutParameter(1, Types.INTEGER);
            stmt.setString(2, email);
            stmt.execute();
            customerId = stmt.getInt(1);

            if (stmt.wasNull()) {
                System.out.println("No customer found with the provided email.");
                customerId = -1;
            }
        } catch (SQLException e) {
            System.out.println("An error occurred while retrieving customer ID ");
            customerId = -1;
        }

        return customerId;
    }

    /**
     * Displays all product categories available in the database.
     */
    public void displayAllCategories() {
        String sql = "{ ? = call customerSide.get_all_categories() }";

        try (CallableStatement stmt = connection.prepareCall(sql)) {
            stmt.registerOutParameter(1, java.sql.Types.ARRAY, "CATEGORY_DETAIL_VARRAY");
            stmt.execute();

            Array array = stmt.getArray(1);
            if (array != null) {
                Object[] categoryDetailsArray = (Object[]) array.getArray();
                for (Object categoryDetailObj : categoryDetailsArray) {
                    Struct categoryDetailStruct = (Struct) categoryDetailObj;
                    Object[] categoryDetailAttributes = categoryDetailStruct.getAttributes();
                    System.out.println("Category Name: " + categoryDetailAttributes[0]);
                }
            }
        } catch (SQLException e) {
            System.out.println("Unable to retrieve categories: " + e.getMessage());
        }
    }

    /**
     * Prints all products belonging to a specific category.
     *
     * @param category the category of the products to be printed
     */

    public void printProductsByCategory(String category) {
        String sql = "{ ? = call customerSide.get_products_by_category(?) }";
        try (CallableStatement stmt = connection.prepareCall(sql)) {
            stmt.registerOutParameter(1, java.sql.Types.ARRAY, "PRODUCT_DETAIL_VARRAY");
            stmt.setString(2, category);

            stmt.execute();
            Array productsArray = (Array) stmt.getObject(1);

            Object[] productDetailsArray = (Object[]) productsArray.getArray();

            for (Object productDetailObj : productDetailsArray) {
                Struct productDetailStruct = (Struct) productDetailObj;
                Object[] productDetailFields = productDetailStruct.getAttributes();

                System.out.println("Product ID: " + productDetailFields[0]);
                System.out.println("Product Name: " + productDetailFields[1]);
                System.out.println("Price: " + productDetailFields[2]);
                System.out.println("Review Description: " + productDetailFields[3]);
                System.out.println("-------------------------------");
            }
        } catch (SQLException e) {
            System.out.println("Unable to retrieve products: " + e.getMessage());
        }
    }

    /**
     * Retrieves the store ID that stocks a specific product.
     *
     * @param productId the ID of the product
     * @return the store ID or -1 if the store is not found
     */
    public int getStoreIdByProduct(int productId) {
        int storeId = -1;
        String sql = "{ ? = call customerSide.get_store_id_by_product(?) }";
        try (CallableStatement stmt = connection.prepareCall(sql)) {
            stmt.registerOutParameter(1, Types.INTEGER);
            stmt.setInt(2, productId);

            stmt.execute();
            storeId = stmt.getInt(1);
            if (stmt.wasNull()) {
                storeId = -1;
            }
        } catch (SQLException e) {
            System.out.println("Unable to retrieve store ID: " + e.getMessage());
            storeId = -1;
        }
        return storeId;
    }

    /**
     * Updates the email address for a customer based on the customer ID.
     *
     * @param customerId the ID of the customer
     * @param newEmail   the new email address to be set
     * @throws SQLException if a database access error occurs
     */

    public void updateCustomerEmail(int customerId, String newEmail) throws SQLException {
        String sql = "{call customerSide.UpdateCustomerEmail(?, ?)}";

        try (CallableStatement stmt = this.connection.prepareCall(sql)) {
            stmt.setInt(1, customerId);
            stmt.setString(2, newEmail);
            stmt.execute();
            System.out.println("Email updated successfully for customer ID: " + customerId);
        } catch (SQLException e) {
            System.out.println("Error occurred while updating email: " + e.getMessage());
            throw e;
        }
    }

    /**
     * Displays all reviews from a customer based on their email
     *
     * @param email the email of the customer
     * @return true if reviews were found and displayed, false otherwise
     */

     public boolean displayReviewByEmail(String email) {
        boolean hasReviews = false;
        CallableStatement callableStatement = null;
        try {
            String callFunction = "{? = call customerSide.GetAllReviewsByEmail(?)}";
            callableStatement = connection.prepareCall(callFunction);
    
            callableStatement.registerOutParameter(1, Types.ARRAY, "REVIEWLIST");
            callableStatement.setString(2, email);  // Set the email parameter
    
            callableStatement.execute();
    
            Array reviewsArray = callableStatement.getArray(1);
            if (reviewsArray != null) {
                Object[] reviews = (Object[]) reviewsArray.getArray();
    
                for (Object review : reviews) {
                    Struct reviewStruct = (Struct) review;
                    Object[] reviewData = reviewStruct.getAttributes();
    
                    System.out.println("Review ID: " + reviewData[0]);
                    System.out.println("Product ID: " + reviewData[1]);
                    System.out.println("Rating: " + reviewData[3]);
                    System.out.println("Rating Flag: " + reviewData[4]);
                    System.out.println("Description: " + reviewData[5]);
                    System.out.println("-----");
    
                    hasReviews = true;
                }
            } else {
                System.out.println("No reviews found for email: " + email);
            }
        } catch (SQLException e) {
            System.out.println("Error executing the GetAllReviewsByEmail function...");
            e.printStackTrace();
        } finally {
            try {
                if (callableStatement != null)
                    callableStatement.close();
            } catch (SQLException e) {
                System.out.println("Error closing the callable statement...");
                e.printStackTrace();
            }
        }
        return hasReviews;
    }
    

    /**
     * Updates the description of an existing review in the database.
     *
     * @param reviewId       the ID of the review to update
     * @param newDescription the new description text for the review
     */

    public void updateReview(int reviewId, String newDescription) {
        CallableStatement callableStatement = null;
        try {
            String procedureCall = "{ call customerSide.UpdateReviewDescription(?, ?) }";
            callableStatement = this.connection.prepareCall(procedureCall);

            callableStatement.setInt(1, reviewId);
            callableStatement.setString(2, newDescription);

            callableStatement.execute();

            System.out.println("Review updated successfully.");

        } catch (SQLException e) {
            System.out.println("Error occurred while updating the review: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (callableStatement != null) {
                try {
                    callableStatement.close();
                } catch (SQLException e) {
                    System.out.println("Error occurred while closing resources: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Deletes a review from the database based on the review ID.
     *
     * @param reviewId the ID of the review to delete
     */

    public void deleteReview(int reviewId) {
        CallableStatement callableStatement = null;
        try {
            String procedureCall = "{ call customerSide.DeleteReview(?) }";
            callableStatement = this.connection.prepareCall(procedureCall);

            callableStatement.setInt(1, reviewId);

            callableStatement.execute();

            System.out.println("Review with ID " + reviewId + " deleted successfully.");
        } catch (SQLException e) {
            System.out.println("Error occurred while deleting the review: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (callableStatement != null) {
                try {
                    callableStatement.close();
                } catch (SQLException e) {
                    System.out.println("Error occurred while closing resources: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Deletes an order from the database based on the order ID.
     *
     * @param orderId the ID of the order to delete
     */

    public void deleteOrder(int orderId) {
        CallableStatement callableStatement = null;
        try {
            String procedureCall = "{ call customerSide.DeleteOrder(?) }";
            callableStatement = this.connection.prepareCall(procedureCall);

            callableStatement.setInt(1, orderId);

            callableStatement.execute();

            System.out.println("Order with ID " + orderId + " deleted successfully.");
        } catch (SQLException e) {
            System.out.println("Error occurred while deleting the order: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (callableStatement != null) {
                try {
                    callableStatement.close();
                } catch (SQLException e) {
                    System.out.println("Error occurred while closing resources: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }





    // MANAGER SIDE
    /**
     * Retrieves a list of all products from the database.
     *
     * @param conn the connection to the database
     * @return an array of all products details
     * @throws SQLException if a database access error occurs
     */

    public Array getProductList(Connection conn) throws SQLException {
        Array array = null;
        try {
            String call = "{ ? = call managerSide.GetProductList() }";
            CallableStatement stmt = conn.prepareCall(call);

            stmt.registerOutParameter(1, Types.ARRAY, "PRODUCT_DETAILS_ARRAY_ALL");

            stmt.execute();

            array = stmt.getArray(1);

            stmt.close();
        } catch (SQLException e) {
            System.out.println("Error occured while retrieving product list: " + e.getMessage());
            throw e;
        }

        return array;
    }

    /**
     * Displays products retrieved from the database.
     *
     * @param conn the connection to the database
     * @throws SQLException if a database access error occurs
     */

    public void displayProducts(Connection conn) throws SQLException {
        try {
            Array array = getProductList(conn);
            ResultSet rs = array.getResultSet();
            while (rs.next()) {
                Struct product = (Struct) rs.getObject(2);
                Object[] productDetails = product.getAttributes();
                System.out.println("ID: " + productDetails[0] + " Name: " + productDetails[1]);

            }
        } catch (SQLException e) {
            System.out.println("Error occurred while retrieving product list: " + e.getMessage());
            throw e;
        }
    }

    /**
     * Deletes a product from the database based on the product ID.
     *
     * @param conn      the connection to the database
     * @param productId the ID of the product to delete
     * @throws SQLException if a database access error occurs
     */

    public void deleteProductFromDatabase(Connection conn, int productId) throws SQLException {
        try {
            String call = "{ call managerSide.DeleteProduct(?) }";
            CallableStatement stmt = conn.prepareCall(call);

            stmt.setInt(1, productId);

            stmt.execute();

            stmt.close();
        } catch (SQLException e) {
            System.out.println("Error occurred while deleting product: " + e.getMessage());
            throw e;
        }
    }

    /**
     * Retrieves an array of all stores from the database.
     *
     * @param conn the connection to the database
     * @return an array of all store details
     * @throws SQLException if a database access error occurs
     */
    public Array getAllStores(Connection conn) throws SQLException {
        Array array = null;
        try {
            String sql = "{? = call managerSide.get_all_stores()}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1, Types.ARRAY, "STORE_DETAIL_ARRAY");
            stmt.execute();
            array = stmt.getArray(1);
            stmt.close();
        } catch (SQLException e) {
            System.out.println("Error occurred while retrieving store listgggg: " + e.getMessage());
            throw e;
        }
        return array;
    }

    /**
     * Displays all stores retrieved from the database.
     *
     * @param conn the connection to the database
     * @throws SQLException if a database access error occurs
     */
    public void displayAllStores(Connection conn) throws SQLException {
        try {
            Array array = getAllStores(conn);
            ResultSet rs = array.getResultSet();
            while (rs.next()) {
                Struct store = (Struct) rs.getObject(2);
                Object[] storeDetails = store.getAttributes();
                System.out.println("Store ID: " + storeDetails[0] + " Store Name: " + storeDetails[1]);
            }
        } catch (SQLException e) {
            System.out.println("Error occurred while retrieving store list: " + e.getMessage());
            throw e;
        }
    }

    /**
     * Retrieves an array of products sold by a specific store.
     *
     * @param conn    the connection to the database
     * @param storeID the ID of the store
     * @return an array of products details
     * @throws SQLException if a database access error occurs
     */

    public Array get_Products_By_Store(Connection conn, int storeID) throws SQLException {
        Array array = null;
        try {
            String sql = "{? = call managerSide.get_products_by_store(?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1, Types.ARRAY, "PRODUCT_DETAIL_VARRAY_STORE");
            stmt.setInt(2, storeID);
            stmt.execute();
            array = stmt.getArray(1);
            stmt.close();
        } catch (SQLException e) {
            System.out.println("Error occured while getting products from a store: " + e.getMessage());
            e.printStackTrace();
        }
        return array;
    }

    /**
     * Displays products sold by a specific store.
     *
     * @param conn    the connection to the database
     * @param storeID the ID of the store
     * @throws SQLException if a database access error occurs
     */

    public void displayProductsFromStore(Connection conn, int storeID) throws SQLException {
        try {
            Array array = get_Products_By_Store(conn, storeID);
            ResultSet rs = array.getResultSet();
            while (rs.next()) {
                Struct productstore = (Struct) rs.getObject(2);
                Object[] products = productstore.getAttributes();
                System.out.println("ID: " + products[0] + " Name " + products[1]);
            }
        } catch (SQLException e) {
            System.out.println("Error occurred while retrieving products from a store: " + e.getMessage());
            throw e;
        }
    }

    /**
     * Updates the price of a product in a specific store.
     *
     * @param conn      the connection to the database
     * @param productId the ID of the product
     * @param storeId   the ID of the store
     * @param price     the new price to be set
     * @throws SQLException if a database access error occurs
     */

    public void updatePriceOfAProduct(Connection conn, int productId, int storeId, int price) throws SQLException {
        try {
            String sql = "{call managerSide.UpdateProductPrice(?,?,?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, storeId);
            stmt.setInt(2, productId);
            stmt.setInt(3, price);
            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            System.out.println("Error occurred while updating price of a product: " + e.getMessage());
            throw e;
        }
    }

    /**
     * Retrieves an array of all warehouse details from the database.
     *
     * @param conn the connection to the database
     * @return an array of STRUCT objects representing warehouse details
     * @throws SQLException if a database access error occurs or the SQL operation
     *                      fails
     */

    public Array getAllWarehouse(Connection conn) throws SQLException {
        Array array = null;
        try {
            String sql = "{? = call managerSide.GetAllWarehouses()}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1, Types.ARRAY, "WAREHOUSE_DETAILS_ARRAY");
            stmt.execute();
            array = stmt.getArray(1);
            stmt.close();
        } catch (SQLException e) {
            System.out.println("Error occurred while retrieving warehouse list: " + e.getMessage());
            throw e;
        }
        return array;
    }

    /**
     * Displays the list of warehouses including their details such as ID and name.
     *
     * @param conn the connection to the database
     * @throws SQLException if a database access error occurs or the SQL operation
     *                      fails
     */

    public void displayWarehouses(Connection conn) throws SQLException {
        try {
            Array array = getAllWarehouse(conn);
            ResultSet rs = array.getResultSet();
            while (rs.next()) {
                Struct warehouses = (Struct) rs.getObject(2);
                Object[] warehouseDetails = warehouses.getAttributes();
                System.out.println("Warehouse ID: " + warehouseDetails[0] + "  Name: " + warehouseDetails[1]);
            }
        } catch (SQLException e) {
            System.out.println("Error occurred while retrieving warehouse list: " + e.getMessage());
            throw e;
        }
    }

    /**
     * Deletes a warehouse from the database based on the warehouse ID.
     *
     * @param conn        the connection to the database
     * @param warehouseID the ID of the warehouse to delete
     * @throws SQLException if a database access error occurs or the SQL operation
     *                      fails
     */

    public void deleteWarehouseFromDatabase(Connection conn, int warehouseID) throws SQLException {
        try {
            String sql = "{call managerSide.DeleteWarehouse(?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, warehouseID);
            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            System.out.println("Error occurred while deleting warehouse: " + e.getMessage());
            throw e;
        }
    }

    /**
     * Updates the quantity of a product in a specific warehouse.
     *
     * @param conn        the connection to the database
     * @param warehouseID the ID of the warehouse where the product is stored
     * @param productID   the ID of the product
     * @param quantity    the new quantity to be set
     * @throws SQLException if a database access error occurs or the SQL operation
     *                      fails
     */

    public void updateProductQuantityInWarehouse(Connection conn, int warehouseID, int productID, int quantity)
            throws SQLException {
        try {
            String sql = "{call managerSide.UpdateProductQuantityInWarehouse(?,?,?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, warehouseID);
            stmt.setInt(2, productID);
            stmt.setInt(3, quantity);
            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            System.out.println("Error occurred while updating product quantity in warehouse: " + e.getMessage());
            throw e;
        }
    }

    /**
     * Displays the quantity of each product in a specific warehouse.
     *
     * @param conn        the connection to the database
     * @param warehouseId the ID of the warehouse
     * @throws SQLException if a database access error occurs or the SQL operation
     *                      fails
     */

    public void displayQtyOfProduct(Connection conn, int warehouseId) throws SQLException {
        try {
            String sql = "{? = call managerSide.DisplayQtyOfProduct(?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1, OracleTypes.CURSOR);
            stmt.setInt(2, warehouseId);
            stmt.execute();
            ResultSet rs = (ResultSet) stmt.getObject(1);
            while (rs.next()) {
                int productId = rs.getInt(1);
                int qty = rs.getInt(2);
                System.out.println("Product ID: " + productId + ", Quantity: " + qty);
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            System.out.println("Error occurred while retrieving product quantity in warehouse: " + e.getMessage());
            throw e;
        }
    }

    /**
     * Displays the audit log from the database which includes actions taken,
     * the dates they were taken, tables modified, and IDs of the modified records.
     *
     * @param conn the connection to the database
     * @throws SQLException if a database access error occurs or the SQL operation
     *                      fails
     */

    public void displayAuditLog(Connection conn) throws SQLException {
        try {
            String sql = "{call managerSide.DisplayAuditLog(?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1, OracleTypes.CURSOR);
            stmt.execute();
            ResultSet rs = (ResultSet) stmt.getObject(1);
            while (rs.next()) {
                int auditId = rs.getInt(1);
                String action = rs.getString(2);
                String date = rs.getString(3);
                String tableModified = rs.getString(4);
                int idModified = rs.getInt(5);
                System.out.println("Audit ID: " + auditId + " Action: " + action + " Date: " + date
                        + " Table Modified: " + tableModified + " ID Modified: " + idModified);
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            System.out.println("Error occurred while retrieving audit log: " + e.getMessage());
            throw e;
        }
    }

}
