package javaportion;

import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author Noah Diplarakis 2244592
 *         The ManagerHelper class provides static methods to facilitate manager
 *         interactions such as creating, deleting, and updating product
 *         information, managing warehouses, and viewing audit logs.
 */

public class ManagerHelper {

    private static Scanner scan = new Scanner(System.in);

    /**
     * Guides the manager through the process of creating a new product in the
     * database.
     *
     * @param conn the StoreListServices instance to use for database operations
     * @throws SQLException           if a database access error occurs
     * @throws ClassNotFoundException if the product class is not found in the Java
     *                                context
     */

    public static void createProduct(StoreListServices conn) throws SQLException, ClassNotFoundException {

        try {
            System.out.println("Enter Product Name: ");
            String nameOfProduct = scan.nextLine();
            System.out.println("Enter Product Category: ");
            String categoryOfProduct = scan.nextLine();
            Product ProductAdded = new Product(nameOfProduct, categoryOfProduct);
            ProductAdded.addToDatabase(conn.getConnection());
            System.out.println("Product created successfully with productName: " + nameOfProduct
                    + " and productCategory " + categoryOfProduct);

        } catch (SQLException e) {
            System.out.println("An error occured while adding a product to the database: " + e.getMessage());
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found: " + e.getMessage());
            e.printStackTrace();
        }

    }

    /**
     * Guides the manager through the process of deleting a product from the
     * database.
     *
     * @param conn the StoreListServices instance to use for database operations
     * @throws SQLException           if a database access error occurs
     * @throws ClassNotFoundException if the product class is not found in the Java
     *                                context
     */

    public static void deleteProduct(StoreListServices conn) throws SQLException, ClassNotFoundException {
        conn.displayProducts(conn.getConnection());
        System.out.println("Enter the ID of the product you want to delete: ");
        int idOfProduct = 0;
        boolean validInput = false;

        while (!validInput) {
            try {
                idOfProduct = Integer.parseInt(scan.nextLine());
                validInput = true;
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a valid number.");
                System.out.println("Enter the ID of the product you want to delete: ");
            }
        }

        conn.deleteProductFromDatabase(conn.getConnection(), idOfProduct);
        System.out.println("Updated Products List:");
        conn.displayProducts(conn.getConnection());
        System.out.println("Product deleted successfully with product ID: " + idOfProduct);
    }

    /**
     * Guides the manager through updating the price of a product in a specified
     * store.
     *
     * @param conn the StoreListServices instance to use for database operations
     * @throws SQLException if a database access error occurs
     */

    public static void updateAProductsPrice(StoreListServices conn) throws SQLException {
        int idOfStore = 0;
        int price = 0;
        int idOfProduct = 0;
        boolean validInput = false;
        conn.displayAllStores(conn.getConnection());
        while (!validInput) {
            try {
                System.out.println("Which store would you like to update a products price");
                idOfStore = Integer.parseInt(scan.nextLine());
                conn.displayProductsFromStore(conn.getConnection(), idOfStore);
                System.out.println("Which Product's price would you like to update?");
                idOfProduct = Integer.parseInt(scan.nextLine());
                System.out.println("What is the price that you want it to be?");
                price = Integer.parseInt(scan.nextLine());
                conn.updatePriceOfAProduct(conn.getConnection(), idOfProduct, idOfStore, price);
                System.out.println("Successfully modified Product ID " + idOfProduct + " of Store " + idOfStore
                        + ". Price is now " + price);
                validInput = true;
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a valid number. For all cases");
            }
        }
    }

    /**
     * Guides the manager through the process of deleting a warehouse from the
     * database.
     *
     * @param conn the StoreListServices instance to use for database operations
     * @throws SQLException if a database access error occurs
     */

    public static void deleteWarehouse(StoreListServices conn) throws SQLException {
        int warehouseId = 0;
        boolean validInput = false;
        while (!validInput) {
            try {
                conn.displayWarehouses(conn.getConnection());
                System.out.println("Which warehouse would you like to delete?");
                warehouseId = Integer.parseInt(scan.nextLine());
                conn.deleteWarehouseFromDatabase(conn.getConnection(), warehouseId);
                System.out.println("Successfully deleted Warehouse " + warehouseId + " from the database");
                validInput = true;
            } catch (NumberFormatException e) {
                System.out.println("Enter a valid input please!: " + e.getMessage());
                e.printStackTrace();
            }
        }

    }

    /**
     * Displays the quantity of products in a specified warehouse.
     *
     * @param conn the StoreListServices instance to use for database operations
     * @throws SQLException if a database access error occurs
     */

    public static void displayWarehouseProductsQty(StoreListServices conn) throws SQLException {
        boolean isValid = false;
        while (!isValid) {
            try {
                conn.displayWarehouses(conn.getConnection());
                System.out.println("Which warehouse would you like to display the products quantity of?");
                int warehouseId = Integer.parseInt(scan.nextLine());
                conn.displayQtyOfProduct(conn.getConnection(), warehouseId);
                ;
                isValid = true;
            } catch (NumberFormatException e) {
                System.out.println("Enter a valid input please!: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    /**
     * Guides the manager through updating the quantity of a specific product in a
     * warehouse.
     *
     * @param conn the StoreListServices instance to use for database operations
     * @throws SQLException if a database access error occurs
     */

    public static void updateAProductsQty(StoreListServices conn) throws SQLException {
        boolean isValid = false;
        while (!isValid) {
            try {
                conn.displayWarehouses(conn.getConnection());
                System.out.println(
                        "Which warehouse would you like to update the quantity of a product?(Type the Warehouse ID)");
                int warehouseId = Integer.parseInt(scan.nextLine());
                conn.displayQtyOfProduct(conn.getConnection(), warehouseId);
                System.out.println("Which product would you like to update the quantity of?");
                int productId = Integer.parseInt(scan.nextLine());
                System.out.println("What is the new quantity of the product?");
                int newQty = Integer.parseInt(scan.nextLine());
                conn.updateProductQuantityInWarehouse(conn.getConnection(), warehouseId, productId, newQty);
                System.out.println("Successfully updated the quantity of product " + productId + " in warehouse "
                        + warehouseId + " to " + newQty);
                isValid = true;
            } catch (NumberFormatException e) {
                System.out.println("Enter a valid input please!: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    /**
     * Displays the audit log from the database.
     *
     * @param conn the StoreListServices instance to use for database operations
     * @throws SQLException if a database access error occurs
     */

    public static void displayAudit(StoreListServices conn) throws SQLException {
        try {
            conn.displayAuditLog(conn.getConnection());
        } catch (SQLException e) {
            System.out.println("An error occured while displaying the audit log: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
