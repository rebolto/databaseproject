package javaportion;

import java.sql.*;
import java.util.Map;

/**
 * The Order class represents a review entity in the SuperStore Database
 * System.
 */

public class Order implements SQLData {
    private int orderId;
    private int customerId;
    private int productId;
    private Date orderDate;
    private int storeId;
    private int productQty;
    private static final String typeName = "ORDER_TYP";

    /**
     * Constructs a new Order with the specified customer ID, product ID, order
     * date, store ID, and product quantity.
     *
     * @param customerId the customer ID associated with this order
     * @param productId  the product ID of the ordered product
     * @param orderDate  the date on which the order was placed
     * @param storeId    the ID of the store from which the order was placed
     * @param productQty the quantity of the product ordered
     */

    public Order(int customerId, int productId, java.sql.Date orderDate, int storeId, int productQty) {
        this.customerId = customerId;
        this.productId = productId;
        this.orderDate = orderDate;
        this.storeId = storeId;
        this.productQty = productQty;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getProductQty() {
        return productQty;
    }

    public void setProductQty(int productQty) {
        this.productQty = productQty;
    }

    @Override
    public String getSQLTypeName() {
        return typeName;
    }

    /**
     * Writes this Order object to a SQL stream, effectively serializing it to be
     * stored in a SQL database.
     *
     * @param stream the SQLOutput stream to which this Order will be written
     * @throws SQLException if a database access error occurs
     */

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getCustomerId());
        stream.writeInt(getProductId());
        stream.writeDate(getOrderDate());
        stream.writeInt(getStoreId());
        stream.writeInt(getProductQty());
    }

    /**
     * Reads data from the given SQL stream to populate this Order object's fields.
     *
     * @param stream   the SQLInput stream from which this Order will be read
     * @param typeName the SQL type name of the data being read
     * @throws SQLException if a database access error occurs
     */

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setCustomerId(stream.readInt());
        setProductId(stream.readInt());
        setOrderDate(stream.readDate());
        setStoreId(stream.readInt());
        setProductQty(stream.readInt());
    }

    /**
     * Returns a string representation of the Order object.
     *
     * @return a string representation of this Order
     */
    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", customerId=" + customerId +
                ", productId=" + productId +
                ", orderDate=" + orderDate +
                ", storeId=" + storeId +
                '}';
    }

    /**
     * Adds this Order object to the database using the given connection.
     * It maps the object to the SQL structured type and calls the respective
     * stored procedure to add the order to the database.
     *
     * @param conn the Connection object to the database
     * @throws SQLException           if a database access error occurs or this
     *                                method is
     *                                called on a closed connection
     * @throws ClassNotFoundException if the ORDER_TYP class is not found in the
     *                                Java context
     */
    public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        Map<String, Class<?>> map = conn.getTypeMap();
        map.put(Order.typeName, Class.forName("javaportion.Order"));
        conn.setTypeMap(map);

        String sql = "{call add_order(?)}";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setObject(1, this);
            stmt.execute();
            System.out.println("Order added successfully to the database.");
        }
    }

}
