package javaportion;

import java.sql.*;
import java.util.Map;

/**
 * The Customer class represents a review entity in the SuperStore Database
 * System.
 */

public class Customer implements SQLData {
    public static final String TYPENAME = "CUSTOMER_TYP";
    private int customerId;
    private String firstname;
    private String lastname;
    private String email;
    private int addressId;

    /**
     * Constructs a new Customer with the given first name, last name, email
     * address, and address ID.
     *
     * @param firstname the first name of the customer
     * @param lastname  the last name of the customer
     * @param email     the email address of the customer
     * @param addressId the address ID of the customer
     */
    public Customer(String firstname, String lastname, String email, int addressId) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.addressId = addressId;
    }

    public int getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAddressId() {
        return this.addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return TYPENAME;
    }

    /**
     * Writes this Customer object to a SQL stream, effectively serializing it to be
     * stored in a SQL database.
     *
     * @param stream the SQLOutput stream to which this Customer will be written
     * @throws SQLException if a database access error occurs
     */
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getCustomerId());
        stream.writeString(getFirstname());
        stream.writeString(getLastname());
        stream.writeString(getEmail());
        stream.writeInt(getAddressId());
    }

    /**
     * Reads data from the given SQL stream to populate this Customer object's
     * fields.
     *
     * @param stream   the SQLInput stream from which this Customer will be read
     * @param typeName the SQL type name of the data being read
     * @throws SQLException if a database access error occurs
     */
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
       setFirstname(stream.readString());
        setLastname(stream.readString());
        setEmail(stream.readString());
        setAddressId(stream.readInt());
        setCustomerId(stream.readInt());
    }

    /**
     * Adds this Customer object to the database using the given connection.
     * It maps the object to the SQL structured type and calls the respective
     * stored procedure to add the customer to the database.
     *
     * @param conn the Connection object to the database
     * @throws SQLException           if a database access error occurs or this
     *                                method is
     *                                called on a closed connection
     * @throws ClassNotFoundException if the CUSTOMER_TYP class is not found in the
     *                                Java context
     */

    public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        try {
            Map<String, Class<?>> map = conn.getTypeMap();

            map.put("CUSTOMER_TYP", Class.forName("javaportion.Customer"));

            conn.setTypeMap(map);

            Customer customerToAdd = new Customer(this.firstname, this.lastname, this.email, this.addressId);

            String sql = "{call add_customer(?)}";
            try (CallableStatement stmt = conn.prepareCall(sql)) {
                stmt.setObject(1, customerToAdd);
                stmt.execute();
            }
        } catch (SQLException e) {
            System.out.println("Error Occurred in addToDatabase for Customer: " + e.getMessage());
            throw e;
        } catch (ClassNotFoundException e) {
            System.out.println("Error Occurred (ClassNotFoundException) for CUSTOMER_TYP: " + e.getMessage());
            throw new SQLException("Type map setup failed for CUSTOMER_TYP", e);
        }
    }

}
