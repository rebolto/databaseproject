package javaportion;

import java.sql.*;
import java.util.Map;

/**
 * The Review class represents a review entity in the SuperStore Database
 * System.
 */

public class Review implements SQLData {
    public static final String TYPENAME = "REVIEW_TYP";
    private int reviewId;
    private int productId;
    private int customerId;
    private int rating;
    private int reviewFlag;
    private String description;

    /**
     * Constructs a new Review with the specified product ID, customer ID, rating,
     * review flag, and description.
     *
     * @param productId   the product ID associated with this review
     * @param customerId  the customer ID associated with this review
     * @param rating      the rating given in this review
     * @param reviewFlag  the flag indicating a status of the review
     * @param description the textual description of the review
     */

    public Review(int productId, int customerId, int rating, int reviewFlag, String description) {
        this.productId = productId;
        this.customerId = customerId;
        this.rating = rating;
        this.reviewFlag = reviewFlag;
        this.description = description;
    }

    public int getReviewId() {
        return this.reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    public int getProductId() {
        return this.productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getRating() {
        return this.rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getReviewFlag() {
        return this.reviewFlag;
    }

    public void setReviewFlag(int reviewFlag) {
        this.reviewFlag = reviewFlag;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return TYPENAME;
    }

    /**
     * Writes this Review object to a SQL stream, effectively serializing it to be
     * stored in a SQL database.
     *
     * @param stream the SQLOutput stream to which this Review will be written
     * @throws SQLException if a database access error occurs
     */

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getReviewId());
        stream.writeInt(getProductId());
        stream.writeInt(getCustomerId());
        stream.writeInt(getRating());
        stream.writeInt(getReviewFlag());
        stream.writeString(getDescription());
    }

    /**
     * Reads data from the given SQL stream to populate this Review object's fields.
     *
     * @param stream   the SQLInput stream from which this Review will be read
     * @param typeName the SQL type name of the data being read
     * @throws SQLException if a database access error occurs
     */

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setProductId(stream.readInt());
        setCustomerId(stream.readInt());
        setRating(stream.readInt());
        setReviewFlag(stream.readInt());
        setDescription(stream.readString());
    }

    /**
     * Adds this Review object to the database using the given connection.
     *
     * @param conn the Connection object to the database
     * @throws SQLException           if a database access error occurs or this
     *                                method is
     *                                called on a closed connection
     * @throws ClassNotFoundException if the REVIEW_TYP class is not found in the
     *                                Java context
     */

    public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        try {
            Map map = conn.getTypeMap();
            conn.setTypeMap(map);
            map.put("REVIEW_TYP", Class.forName("javaportion.Review"));
            Review reviewToAdd = new Review(this.productId, this.customerId, this.rating, this.reviewFlag,
                    this.description);
            String sql = "{call add_review(?)}";
            try (CallableStatement stmt = conn.prepareCall(sql)) {
                stmt.setObject(1, reviewToAdd);
                stmt.execute();
            }
        } catch (SQLException e) {
            System.out.println("Error Occured in AddToDatabase Review");
        } catch (ClassNotFoundException a) {
            System.out.println("Error Occured (ClassNotFoundExpection) for Review");
        }
    }

}
