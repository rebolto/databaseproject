package javaportion;

import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author Ivana Zhekova 2038099 & Noah Diplarakis 2244592
 * 
 *         This class serves as the main entry point for the SuperStore
 *         Management System, providing command-line interfaces for both customers and managers to
 *         interact with the system. The class handles user authentication, presents menus for
 *         different user roles, and facilitates database operations through the StoreListServices class.
 */

 
public class App {
    private static Scanner scan = new Scanner(System.in);

    // CUSTOMER SIDE

    /**
     * Handles the customer interface, allowing new and returning customers to
     * perform various actions.
     *
     * @param conn The StoreListServices object to facilitate interaction with the
     *             database.
     * @throws SQLException if a database access error occurs.
     */

    private static void handleCustomer(StoreListServices conn) throws SQLException {
        System.out.println("Are you a returning customer or new? (returning/new)");
        String customerType;
        String userEmail = "";

        while (true) {
            customerType = scan.nextLine().trim().toLowerCase();

            if ("new".equals(customerType)) {
                CustomerHelper.createCustomer(conn);
                System.out.println("Enter your email to continue:");
                userEmail = scan.nextLine();
                break;
            } else if ("returning".equals(customerType)) {
                System.out.println("Enter your email:");
                userEmail = scan.nextLine();
                break;
            } else {
                System.out.println("Invalid customer type selected. Please enter 'returning' or 'new':");
            }
        }

        while (true) {
            System.out.println("What would you like to do?");
            System.out.println("1 - Place a new order");
            System.out.println("2 - Write a review");
            System.out.println("3 - Update your email");
            System.out.println("4 - Update a review");
            System.out.println("5 - Delete a review");
            System.out.println("6 - Delete an order");
            System.out.println("7 - Exit");

            int action;
            try {
                action = Integer.parseInt(scan.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a number between 1 and 7.");
                continue;
            }

            switch (action) {
                case 1:
                    CustomerHelper.createNewOrder(conn, userEmail);
                    break;
                case 2:
                    CustomerHelper.createReview(conn, userEmail);
                    break;
                case 3:
                    CustomerHelper.updateEmail(conn, userEmail);
                    break;
                case 4:
                    CustomerHelper.updateReview(conn, userEmail);
                    break;
                case 5:
                    CustomerHelper.deleteCustomerReview(conn, userEmail);
                    break;
                case 6:
                    CustomerHelper.deleteOrder(conn, userEmail);
                    break;
                case 7:
                    System.out.println("Thank you for using the Store Management System. Goodbye!");
                    return;
                default:
                    System.out.println("Invalid action selected. Please select a number between 1 and 7.");
                    break;
            }
        }
    }

    // MANAGER SIDE

    /**
     * Handles the manager interface, allowing managers to perform administrative
     * actions such as adding or deleting products, managing warehouses, updating
     * product
     * information, and viewing audit logs.
     *
     * @param conn The StoreListServices object to facilitate interaction with the
     *             database.
     * @throws SQLException           if a database access error occurs.
     * @throws ClassNotFoundException if the JDBC class is not found.
     */

    public static void handleManager(StoreListServices conn) throws SQLException, ClassNotFoundException {
        boolean exit = false;
        while (!exit) {
            System.out.println("What would you like to do?");
            System.out.println("0 - Add a Product");
            System.out.println("1 - Delete a Product");
            System.out.println("2 - Delete a Warehouse");
            System.out.println("3 - Update a Product's Price");
            System.out.println("4 - Update the quantity of a Product in a Warehouse");
            System.out.println("5 - Display the Quantity of Products in a Warehouse");
            System.out.println("6 - Display the Audit Logs");
            System.out.println("7 - Exit the application");

            int action;
            try {
                action = Integer.parseInt(scan.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a number between 0 and 7.");
                continue;
            }

            switch (action) {
                case 0:
                    ManagerHelper.createProduct(conn);
                    break;
                case 1:
                    ManagerHelper.deleteProduct(conn);
                    break;
                case 2:
                    ManagerHelper.deleteWarehouse(conn);
                    break;
                case 3:
                    ManagerHelper.updateAProductsPrice(conn);
                    break;
                case 4:
                    ManagerHelper.updateAProductsQty(conn);
                    break;
                case 5:
                    ManagerHelper.displayWarehouseProductsQty(conn);
                    break;
                case 6:
                    ManagerHelper.displayAudit(conn);
                    break;
                case 7:
                    exit = true;
                    System.out.println("Thank you for using the Store Management System. Goodbye!");
                    break;
                default:
                    System.out.println("Invalid action selected. Please select a number between 0 and 7.");
                    break;
            }
        }
    }

    public static void main(String[] args) {

       
        System.out.println("Welcome to the SuperStore Management System");

         String user = new String(System.console().readLine("Enter your database username: "));
         String pwd = new String(System.console().readPassword("Enter your database password: "));

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");

            StoreListServices conn = new StoreListServices(user, pwd);

            Scanner scan = new Scanner(System.in);
            boolean validRoleSelected = false;

            while (!validRoleSelected) {
                System.out.println("Are you here as a customer or a manager?(customer/manager)");
                String role = scan.nextLine().toLowerCase();

                switch (role) {
                    case "customer":
                        handleCustomer(conn);
                        validRoleSelected = true;
                        break;
                    case "manager":
                        handleManager(conn);
                        validRoleSelected = true;
                        break;
                    default:
                        System.out.println("Invalid role selected. Please try again.");
                        break;
                }
            }

        } catch (ClassNotFoundException e) {
            System.out.println("Oracle JDBC Driver not found");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("A SQL error occurred: " + e.getMessage());
            e.printStackTrace();
        } finally {
            scan.close();
        }
    }

}
