package javaportion;

import java.sql.*;
import java.util.Map;

    /**
     * The Product class represents a review entity in the SuperStore Database
     * System.
     */

public class Product implements SQLData {

    private int productId;
    private String productName;
    private String productCategory;
    private static final String typeName = "PRODUCT_TYP";

    /**
     * Constructs a new Product with the specified name and category.
     *
     * @param productName     the name of the product
     * @param productCategory the category of the product
     */

    public Product(String ProductName, String ProductCategory) {
        this.productName = ProductName;
        this.productCategory = ProductCategory;
    }

    @Override
    public String getSQLTypeName() {
        return typeName;
    }

    public int getProductID() {
        return this.productId;
    }

    public String getProductName() {
        return this.productName;
    }

    public String getProductCategory() {
        return this.productCategory;
    }

    public void setProductId(int product) {
        this.productId = product;
    }

    public void setProductName(String productname) {
        this.productName = productname;
    }

    public void setProductCategory(String category) {
        this.productCategory = category;
    }

    /**
     * Writes this Product object to a SQL stream, effectively serializing it to be
     * stored in a SQL database.
     *
     * @param stream the SQLOutput stream to which this Product will be written
     * @throws SQLException if a database access error occurs
     */

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getProductID());
        stream.writeString(getProductName());
        stream.writeString(getProductCategory());

    }

    /**
     * Reads data from the given SQL stream to populate this Product object's
     * fields.
     *
     * @param stream   the SQLInput stream from which this Product will be read
     * @param typeName the SQL type name of the data being read
     * @throws SQLException if a database access error occurs
     */

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setProductId(stream.readInt());
        setProductName(stream.readString());
        setProductCategory(stream.readString());
    }

    /**
     * Returns a string representation of the Product object.
     *
     * @return a string representation of this Product
     */

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productName=" + productName +
                ", productCategory=" + productCategory +
                "}";
    }

    /**
     * Adds this Product object to the database using the given connection.
     *
     * @param conn the Connection object to the database
     * @throws SQLException           if a database access error occurs or this
     *                                method is
     *                                called on a closed connection
     * @throws ClassNotFoundException if the PRODUCT_TYP class is not found in the
     *                                Java context
     */

    public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        try {
            Map<String, Class<?>> map = conn.getTypeMap();
            conn.setTypeMap(map);
            map.put("PRODUCT_TYP", Class.forName("javaportion.Product"));
            Product productToBeAdded = new Product(this.productName, this.productCategory);
            String sql = "{call add_product(?)}";
            try (CallableStatement stmt = conn.prepareCall(sql)) {
                stmt.setObject(1, productToBeAdded);
                stmt.execute();
            }
        } catch (SQLException e) {
            System.out.println("Error occurred in addToDatabase for Product: " + e.getMessage());
            throw e;
        } catch (ClassNotFoundException e) {
            System.out.println("Error occurred (ClassNotFoundException) for PRODUCT_TYP: " + e.getMessage());
            throw new SQLException("Type map setup failed for PRODUCT_TYP", e);
        }

    }
}