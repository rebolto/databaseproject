package javaportion;

import java.sql.SQLException;
import java.util.Date;
import java.util.Scanner;

/**
 * @author Ivana Zhekova 2038099
 *         The CustomerHelper class provides static methods to facilitate
 *         customer
 *         interaction such as creating orders, writing reviews, and updating
 *         customer information.
 */

public class CustomerHelper {

    private static Scanner scan = new Scanner(System.in);

    /**
     * Guides the user through the process of creating a new order.
     *
     * @param conn      the StoreListServices instance to use for database
     *                  operations
     * @param userEmail the email of the user to create the order for
     */

    public static void createNewOrder(StoreListServices conn, String userEmail) {
        try {
            conn.displayAllCategories();

            System.out.println("Please enter the category you want to order from:");
            String category = scan.nextLine();

            conn.printProductsByCategory(category);

            System.out.println("Please enter the product ID you want to order:");
            int productId = Integer.parseInt(scan.nextLine());

            int customerId = conn.getCustomerIdByEmail(userEmail);
            if (customerId == -1) {
                System.out.println("Customer ID not found for the email: " + userEmail);
                return;
            }

            int storeId = conn.getStoreIdByProduct(productId);
            if (storeId == -1) {
                System.out.println("Store ID not found for the product ID: " + productId);
                return;
            }

            System.out.println("Please enter the quantity you want to order:");
            int productQty = Integer.parseInt(scan.nextLine());

            Date orderDate = new java.sql.Date(System.currentTimeMillis());

            Order order = new Order(customerId, productId, (java.sql.Date) orderDate, storeId, productQty);

            order.addToDatabase(conn.getConnection());
            System.out.println("Order created successfully.");

        } catch (Exception e) {
            System.out.println("An error occurred: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Guides the user through the process of creating a review for a product.
     *
     * @param conn      the StoreListServices instance to use for database
     *                  operations
     * @param userEmail the email of the user to create the review for
     */

    public static void createReview(StoreListServices conn, String userEmail) {
        try {
            boolean ordersExist = conn.displayCustomerOrders(userEmail);
            if (!ordersExist) {
                return;
            }
            int customerId = conn.getCustomerIdByEmail(userEmail);
            if (customerId == -1) {
                System.out.println("Customer ID not found for the email: " + userEmail);
                return;
            }

            System.out.println("Please enter the product ID you want to review:");
            int productId = Integer.parseInt(scan.nextLine());

            System.out.println("Please enter your rating (1-5):");
            int rating = Integer.parseInt(scan.nextLine());

            System.out.println("Do you want to flag this review? (yes/no):");
            String flagResponse = scan.nextLine().trim().toLowerCase();
            int reviewFlag = flagResponse.equals("yes") ? 1 : 0;

            System.out.println("Please enter your review description:");
            String description = scan.nextLine();

            Review review = new Review(productId, customerId, rating, reviewFlag, description);

            review.addToDatabase(conn.getConnection());
            System.out.println("Review added successfully.");

        } catch (SQLException e) {
            System.out.println("A SQL error occurred: " + e.getMessage());
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("A formatting error occurred, please enter numeric values for product ID and rating.");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("An error occurred: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Guides the user through the process of creating a new customer record.
     *
     * @param conn the StoreListServices instance to use for database operations
     */

    public static void createCustomer(StoreListServices conn) {
        try {
            System.out.println("Please enter the first name:");
            String firstname = scan.nextLine();

            System.out.println("Please enter the last name:");
            String lastname = scan.nextLine();

            System.out.println("Please enter the email:");
            String email = scan.nextLine();

            System.out.println("Email entered: " + email);

            System.out.println("Enter street:");
            String street = scan.nextLine();

            System.out.println("Enter city:");
            String city = scan.nextLine();

            System.out.println("Enter province:");
            String province = scan.nextLine();

            System.out.println("Enter country:");
            String country = scan.nextLine();

            CompleteAddress address = new CompleteAddress(street, city, province, country);
            address.addToDatabase(conn.getConnection());

            int addressId = address.getAddressId();

            Customer customer = new Customer(firstname, lastname, email, addressId);
            customer.addToDatabase(conn.getConnection());

            System.out.println("Customer created successfully with address ID: " + addressId);
        } catch (SQLException e) {
            System.out.println("An error occurred while adding the customer to the database: " + e.getMessage());
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Please enter a valid number for the address ID.");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Allows a user to update their email address.
     *
     * @param conn      the StoreListServices instance to use for database
     *                  operations
     * @param userEmail the current email of the user to be updated
     * @throws SQLException if a database access error occurs
     */

    public static void updateEmail(StoreListServices conn, String userEmail) throws SQLException {
        int customerId = conn.getCustomerIdByEmail(userEmail);
        if (customerId == -1) {
            System.out.println("Customer ID not found for the email: " + userEmail);
            return;
        }
        System.out.println("Enter new email:");
        String newEmail = scan.nextLine();
        conn.updateCustomerEmail(customerId, newEmail);
        System.out.println("Email updated successfully.");
    }

    /**
     * Allows a user to update an existing review.
     *
     * @param conn      the StoreListServices instance to use for database
     *                  operations
     * @param userEmail the email of the user whose review is to be updated
     */

    public static void updateReview(StoreListServices conn, String userEmail) {
        Scanner scanner = new Scanner(System.in);
        int customerId = conn.getCustomerIdByEmail(userEmail);
        try {
            boolean reviewsExist = conn.displayReviewByEmail(userEmail);
            if (!reviewsExist) {
                System.out.println("No reviews found for the provided customer ID.");
                return;
            }
            System.out.println("Enter the ReviewId you want to update: ");
            int reviewId = scanner.nextInt();

            scanner.nextLine();
            System.out.println("Enter the new description for the review: ");
            String newDescription = scanner.nextLine();
            conn.updateReview(reviewId, newDescription);

            System.out.println("Review updated successfully.");

        } catch (Exception e) {
            System.out.println("An error occurred: " + e.getMessage());
            e.printStackTrace();
        } finally {

        }
    }

    /**
     * Allows a user to delete their review.
     *
     * @param conn      the StoreListServices instance to use for database
     *                  operations
     * @param userEmail the email of the user whose review is to be deleted
     */

    public static void deleteCustomerReview(StoreListServices conn, String userEmail) {
        Scanner scanner = new Scanner(System.in);
        int customerId = conn.getCustomerIdByEmail(userEmail);

        try {
            boolean reviewsExist = conn.displayReviewByEmail(userEmail);
            if (!reviewsExist) {
                System.out.println("No reviews found for the provided customer ID.");
                return;
            }
            System.out.println("Enter the ReviewId of the review you want to delete: ");
            int reviewId = scanner.nextInt();

            conn.deleteReview(reviewId);

            System.out.println("Review with ID " + reviewId + " was deleted successfully.");

        } catch (Exception e) {
            System.out.println("An error occurred: " + e.getMessage());
            e.printStackTrace();
        } finally {
        }
    }

    /**
     * Allows a user to delete an order.
     *
     * @param conn      the StoreListServices instance to use for database
     *                  operations
     * @param userEmail the email of the user whose order is to be deleted
     */

    public static void deleteOrder(StoreListServices conn, String userEmail) {
        Scanner scanner = new Scanner(System.in);
        int customerId = conn.getCustomerIdByEmail(userEmail);

        try {
            boolean ordersExist = conn.displayCustomerOrders(userEmail);
            if (!ordersExist) {
                return;
            }
            System.out.println("Enter the OrderId of the order you want to delete: ");
            int orderId = scanner.nextInt(); 

            conn.deleteOrder(orderId);

            System.out.println("Order with ID " + orderId + " was deleted successfully.");

        } catch (Exception e) {
            System.out.println("An error occurred: " + e.getMessage());
            e.printStackTrace();
        } finally {
        }
    }

}
